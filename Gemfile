source 'https://rubygems.org'

# Use haml for markup
gem 'haml'
# User Devise for authentication
gem 'devise'
# Use Pundit for authorization
gem 'pundit'
# Use FactoryGirl for test fixtures
gem 'factory_girl_rails', :require => false
# For cleaning the db during tests
gem 'database_cleaner'
# For creating fake data in FactoryGirl
gem 'faker'
# Use bootstrap
gem 'twitter-bootstrap-rails'
gem 'bootstrap_form'
# Needed by twitter-bootstrap-rails
gem 'less-rails'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5.2'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer',  platforms: :ruby
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# Error notification for devs
gem 'exception_notification'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc
# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development
# Deployment
gem 'capistrano'
gem 'capistrano-passenger'
gem 'capistrano-bundler'
gem 'capistrano-rails'
gem 'capistrano-rbenv'
# Email during development
gem 'mailcatcher'
# Use application.yml to set environment variables
gem 'figaro'
# Typeahead searching
gem 'twitter-typeahead-rails'
# Datepicker
gem 'bootstrap-datepicker-rails'
# Use unicorn as the app server in development: pdfkit needs multi-threading
gem 'unicorn', group: :development
# https://github.com/pdfkit/pdfkit
gem 'pdfkit'
# For handling file uploads
gem 'paperclip'
# Fancy tables
gem 'bootstrap-table-rails'
# For linear gradiants
gem 'compass-rails'
# Table sorting
gem 'jquery-tablesorter'
# Test coverage
gem 'simplecov', :require => false, :group => :test
# For hierarchical models
gem 'acts_as_tree'
# For Google recaptcha
gem 'recaptcha', require: 'recaptcha/rails'

group :development, :test do
  # Use rspec for testing
  gem 'rspec-rails'
  # Pretty printing for the console
  gem 'awesome_print'
  # Needed to rename the app
  gem 'rename'
end
