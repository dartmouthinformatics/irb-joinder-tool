module DownloadsHelper

  def unchecked_box
    content_tag :span, '&#x2610'.html_safe, style: 'font-size:20px;'
  end

  def checked_box
    content_tag :span, '&#x2611'.html_safe, style: 'font-size:20px;'
  end

  def bullet
    content_tag :span, '&#149'.html_safe, style: 'font-size:20px;'
  end

end
