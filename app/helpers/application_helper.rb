module ApplicationHelper

  # Used throught the app to display the app name.
  def app_name
    'SMART IRB'
  end
 
  # Adds the "error" class to field if it's resource has an error for the field
  # (Used for highlighting errors)
  # http://stackoverflow.com/questions/12222408/how-to-highlight-fields-on-rails-validation-errors
  def field_class(resource, field_name)
    if resource.errors.messages[field_name] && resource.errors.messages[field_name].length > 0
      return 'form_error'.html_safe
    else
      return ''.html_safe
    end
  end  

  # For adding a * character to required form fields
	def required
		content_tag :span, '*', class: 'red', style: 'font-size:20px;'
	end

	# Example: Mon, 09 Nov 2009 00:00:00 +0000 => November 9, 2009
	def format_date(date)
		date.strftime('%B %-d, %Y') if date
	end

	# Example: Mon, 09 Nov 2009 00:00:00 +0000 => 11/09/09 12:00 AM
	def format_date_time(date_time)
		date_time.strftime('%m/%d/%y %I:%M %p') if date_time
	end

  # Adds a right arrow to a sidebar item when appropriate
  def sidebar_arrow(step)
    c = params[:controller]
    a = params[:action]
    arrow = '&#x27a1;'
    # Step is one of:
    #   Choose Institution
    #   Profile
    #   Registration
    #   Agreement Upload
    #   Submit Agreement
    case step
    when 'Choose Institution'
      if c == 'fwas' && ['start', 'search', 'search_results', 'select'].include?(a)
        return arrow
      else
        return '&nbsp;'
      end
    when 'Profile'
      if c == 'users' && ['complete_profile', 'save_profile'].include?(a)
        return arrow
      else
        return '&nbsp;'
      end
    when 'Registration'
      if c == 'institutions' && ['register', 'update'].include?(a)
        return arrow
      else
        return '&nbsp;'
      end
    when 'Agreement Upload'
      if ['downloads', 'uploads'].include?(c) && ['offer_signatory', 'new', 'index'].include?(a)
        return arrow
      else
        return '&nbsp;'
      end
    when 'Submit Agreement'
      if c == 'submissions' && a == 'new'
        return arrow
      else
        return '&nbsp;'
      end
    end
  end

  # Highlights a step in the progress bar as current when appropriate
  def progress_bar_current(step)
    c = params[:controller]
    a = params[:action]
    # Step is one of:
    #   Choose Institution
    #   Profile
    #   Registration
    #   Agreement Upload
    #   Submit Agreement
    case step
    when 'Choose Institution'
      if c == 'fwas' && ['start', 'search', 'search_results', 'select'].include?(a)
        return 'current'
      else
        return ''
      end
    when 'Profile'
      if c == 'users' && ['complete_profile', 'save_profile'].include?(a)
        return 'current'
      else
        return ''
      end
    when 'Registration'
      if c == 'institutions' && ['update', 'register'].include?(a)
        return 'current'
      else
        return ''
      end
    when 'Agreement Upload'
      if ['downloads', 'joinder_uploads'].include?(c) && ['offer_signatory', 'new', 'index'].include?(a)
        return 'current'
      else
        return ''
      end
    when 'Submit Agreement'
      if c == 'submissions' && a == 'new'
        return 'current'
      else
        return ''
      end
    end
  end

  def us_states
      [
        '',
        'AL',
        'AK',
        'AZ',
        'AR',
        'CA',
        'CO',
        'CT',
        'DE',
        'DC',
        'FL',
        'GA',
        'HI',
        'ID',
        'IL',
        'IN',
        'IA',
        'KS',
        'KY',
        'LA',
        'ME',
        'MD',
        'MA',
        'MI',
        'MN',
        'MS',
        'MO',
        'MT',
        'NE',
        'NV',
        'NH',
        'NJ',
        'NM',
        'NY',
        'NC',
        'ND',
        'OH',
        'OK',
        'OR',
        'PA',
        'PR',
        'RI',
        'SC',
        'SD',
        'TN',
        'TX',
        'UT',
        'VT',
        'VA',
        'WA',
        'WV',
        'WI',
        'WY'
      ]
  end

end
