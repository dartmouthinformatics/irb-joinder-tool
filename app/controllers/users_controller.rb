# This class handles assigning instutions to users and editing user profiles.
# NOTE: Institution records are created here, when a user selects a previously unselected FWA.
class UsersController < ApplicationController

  before_action :only_admins, only: [:update_active]

  # Associates a user with an Institution via the user having selected an FWA.
  def assign_institution
    # Do not proceed if current_user already has an institution (back-button handling)
    # NOTE: This precludes any reassigning of Institutions to Users
    unless current_user.institution
      fwa_id = params[:id]
      # Check for an existing Institution record
      institution = Institution.find_by(fwa_id: fwa_id)
      # Create a new Institution record if one does not exist for this FWA
      institution = create_institution(fwa_id) unless institution
      # Associate the user with the Institution
      current_user.update_attributes(institution_id: institution.id)
    end
    redirect_to root_url
  end

  # Displays a form that allows existing users to complete their profile.
  # Degrees is stored as a CSV string, but used as an array in forms.
  def complete_profile
    @user = current_user
    @downloader = Downloader.find_by(email: @user.email)
    @degrees = current_user.degrees ? current_user.degrees.split(',') : []
  end

  # Destination from complete_profile form.
  # Saves (updates) a User with information from that form.
  def save_profile
    @user = current_user
    @user.completing_profile = true
    @profile_params = profile_params
    # Convert degrees from an array into a CSV string before saving.
    @profile_params[:degrees] = @profile_params[:degrees].join(',') if @profile_params[:degrees]
    if @user.update_attributes(@profile_params)
      flash.notice = 'Account profile successfully completed.'
      redirect_to :root
    else
      flash.now.alert = @user.errors.full_messages.join("<br>").html_safe
      @downloader = Downloader.find_by(email: @user.email)
      @degrees = @user.degrees ? current_user.degrees.split(',') : []
      render :complete_profile
    end
  end

  # Allows signed-in users to download the SMART IRB agreement.
  def download_agreement
    send_file "#{Rails.root}/agreement/SMART_IRB_Agreement.pdf"
  end

  # Sends an email from the user to all admins
  def contact
    UserMailer.contact(current_user.email, params[:subject], params[:email_body]).deliver_now
    flash.notice = 'Your email has been sent.'
    redirect_to :back
  end

  # Updates user status as active/inactive. Admins only
  def update_active
    user = User.find(params[:user_id])
    user.update_attributes(active: !user.active)
    render text: 'true'
  end

  private

  # Creates an Institution record when the first user for an Institution selects
  # an FWA. Some information is redundant with the FWA so that it can be edited
  # for the institution if needed.
  def create_institution(fwa_id)
    fwa = Fwa.find(fwa_id)
    institution = Institution.new
    institution.fwa_id = fwa.id
    institution.creating_user_id = current_user.id
    institution.name = fwa.name
    institution.city = fwa.city
    institution.state = fwa.state
    institution.save
    institution
  end

  # Whitelisted parameters for updating User objects.
  def profile_params
    params.require(:user).permit(:first_name, :last_name, :phone, :title, degrees: [])
  end

end
