# This class handles the creation and editing of Submission records.
class SubmissionsController < ApplicationController

  before_action :check_for_upload, only: [:new, :create]
  before_action :check_for_submission, only: [:new, :create]
  before_action :only_admins, only: :update

  # Displays a Submission creation form
  def new
    @submission = Submission.new
    @upload = current_user.institution.uploads.joinder.order(:created_at).last
  end

  # Saves a new Submission recored
  def create
    @submission_params = submission_params
    @submission_params[:institution_id] = current_user.institution.id
    @submission_params[:created_by] = current_user.id
    @submission_params[:status] = 'needs_review'
    @submission = Submission.new(@submission_params)
    if @submission.save
      redirect_to :root, status: 303
      UserMailer.new_submission(current_user).deliver_now
      AdminMailer.new_submission(current_user).deliver_now
    else
      flash.now.alert = @submission.errors.full_messages.join("<br>").html_safe
      render :new
    end
  end

  # Updates an existing Submission recored (use submission_path(submisssion) and PATCH)
  # Expects an ID param to find the submission. Only admins and superadmins have access
  # to this method. User updates to submissions are handled in the edit method.
  # TODO: This method is too complex and should be refactored.
  def update
    submission = Submission.find(params[:id])
    @submission_params = submission_params
    # Handle assigning approver_id (current_user) when appropriate
    if @submission_params[:status]
      new_status = @submission_params[:status]
      if new_status == 'approved'
        @submission_params[:approver_id] = current_user.id
      else
        @submission_params[:approver_id] = nil
      end
    end
    email_body = params[:email_body]
    # Make sure that an email body was passed in iff we are changing the submission status
    if email_body.blank? && @submission_params[:status]
      flash.alert = 'Please fill out the email body.'
      redirect_to :root, status: 303
      return
    end
    # Attempt to update the submission record if we got this far
    if submission.update_attributes(@submission_params)
      # Only send the email if submission status is being changed
      if @submission_params[:status]
        # Set the submission.last_status_note for display when the user logs back in
        submission.update_attributes(last_status_note: email_body)
        # Send email regarding status change
        UserMailer.submission_status_changed(submission, email_body, current_user).deliver_now
        flash.notice = 'Submission status successfully updated. Email sent.'
      else
        # NOTE: For now, the only updates to submissions other than status changes are changes
        # to submission.notes. Other future cases will require more conditionals.
        flash.notice = 'Submission notes successfully updated.'
      end
      redirect_to :root, status: 303
    else
      flash.alert = submission.errors.full_messages.join("<br>").html_safe
      redirect_to :root, status: 303
    end
  end

  # Allows users to update the upload that is associated with a submission after 
  # receiving a status of 'Needs Revision'
  def edit
    begin
      @submission = Submission.find(params[:id])
    rescue
      flash.alert = 'Could not find a submission with that ID'
      redirect_to :root
      return
    end
    @upload = current_user.institution.uploads.order(:created_at).last
    unless (current_user.institution == @submission.institution) || current_user.privileged?
      flash.alert = 'Not authorized to view this submission'
      redirect_to :root
    end
  end

  # Saves updated upload_id for a submission
  def save_edit
    @submission_params = submission_params
    submission_id = @submission_params[:id]
    upload_id = @submission_params[:upload_id]
    @submission = Submission.find(submission_id)
    if @submission.update_attributes(upload_id: upload_id, status: 0, created_by: current_user.id)
      flash.notice = 'Submission successfully updated!'
      redirect_to :root, status: 303
    else
      flash.alert = @submission.errors.full_messages.join("<br>").html_safe
      redirect_to :root, status: 303
    end
  end

  private

  # Whitelisted Submission parameters
  def submission_params
    params.require(:submission).permit(:id, :status, :notes, :upload_id)
  end

  # Before filter for new and create methods.
  # Users should not already be associated with a submission when those actions are called.
  def check_for_submission
    redirect_to root_url if current_user.institution.submission
  end

  # Before filter for new and create methods
  # Users should can not create a submission unless their instituion has an upload
  def check_for_upload
    redirect_to root_url unless current_user.institution.uploads.first
  end

end
