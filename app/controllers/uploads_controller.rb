# NOTE: This controller is only used for the currently-disabled Document Management System
# See the JoinderUploads controller for uploads related to a Joinder submission
# This class handles the creation and downloading of uploaded documents
class UploadsController < ApplicationController

  # Create a new upload and save it to the database
  def create
    @upload_params = upload_params
    @upload_params[:user_id] = current_user.id
    @upload = Upload.new(@upload_params)
    if current_user.can_write_to_folder?(Folder.find(@upload.folder_id))
      if @upload.save
        # no-op
      else
        # Delete redundant error
        @upload.errors.delete :doc
        flash.alert = @upload.errors.full_messages.join("<br>").html_safe
      end
    else
      flash.alert = 'Not authorized to create documents in this folder'
    end
    redirect_to folder_path(@upload.folder_id)
  end

  # Only used for renaming uploads as of now
  def update
    @upload_params = upload_params
    # Do not allow spaces
    if @upload_params[:doc_file_name]
      @upload_params[:doc_file_name] = @upload_params[:doc_file_name].gsub(' ', '_')
    end
    @upload = Upload.find(@upload_params[:id])
    if current_user.can_write_to_folder?(@upload.folder)
      original_file_with_path = "#{Rails.root}/uploads/#{@upload.id}/#{@upload.doc_file_name}"
      new_file_with_path = "#{Rails.root}/uploads/#{@upload.id}/#{@upload_params[:doc_file_name]}"
      if @upload.update_attributes(@upload_params)
        FileUtils.mv original_file_with_path, new_file_with_path
      else
        flash.alert = @upload.errors.full_messages.join("<br>").html_safe
      end
    else
      flash.alert = 'Not authorized to modify this file'
    end
    redirect_to folder_path(@upload.folder.id)
  end

  # Destroys an upload
  def destroy
    upload = Upload.find(params[:id])
    if current_user.can_write_to_folder?(upload.folder)
      upload.destroy
    else
      flash.alert = 'Not authorized to delete this file'
    end
    redirect_to folder_path(upload.folder.id)
  end

  # Render an upload to the browser
  def download
    begin
      upload = Upload.find(params[:id])
    rescue
      flash.now.alert = 'Could not find a file with that ID'
      redirect_to :back
      return
    end
    file_with_path = "#{Rails.root}/uploads/#{upload.id}/#{upload.doc_file_name}"

    # Determine authorization
    authorized = false
    if upload.kind == 'study'
      folder = upload.folder
      if current_user.can_read_folder?(folder)
        authorized = true
      end
    end
    
    if authorized
      send_file file_with_path, disposition: 'inline'
    else
      flash.alert = 'Not Authorized to view this file'
      redirect_to :back
    end
  end

  private

  # Whitelist of parameters for creating an upload.
  def upload_params
    params.require(:upload).permit(:id, :doc_file_name, :doc, :kind, :folder_id)
  end

end
