# This is an ovverride of Devise's RegistrationsController
# It was necessary becuase notifications were not being given
# after successful user signup. This was due to using confirmable,
# which results in two redirects, killing the flash.
class RegistrationsController < Devise::RegistrationsController
  before_action :check_signed_in, only: [:new_from_token, :create]

  # Makes sure user isn't already signed in
  def check_signed_in
    if user_signed_in?
      reset_session
      flash.alert = 'You cannot register a new user while already logged in as another user.'
      redirect_to new_user_session_path
    end
  end

  def new
    # NOTE: open sign up is disabled
    # super
  end

  # Handles new registrations. Custom method is needed because a Downloader-identifying
  # token is passed in in the URL
  def new_from_token
    @downloader = Downloader.find_by(token: params[:token])
    # Return to root if Downloader is not found or token has been used
    if @downloader
      if @downloader.token_used_at
        flash.alert = 'Link has already been used to register a user.'
        redirect_to :new_user_session
        return
      end
    else
      flash.alert = 'Could not find a person to register using that link.'
      redirect_to :new_user_session
      return
    end
    build_resource({})
    set_minimum_password_length
    yield resource if block_given?
    respond_with self.resource
  end

  # Sets token_used_at before deferring to the default Devise create method
  def create
    if params[:token]
      @downloader = Downloader.find_by(token: params[:token])
      if @downloader
        # Set token_used_at so that token can not be used again
        @downloader.update_attributes(token_used_at: Time.now)
      else
        flash.alert = 'Could not find a person to register using that link.'
        redirect_to :new_user_session
        return
      end
    end
    super
  end

  # Defers to the default Devise method
  def update
    # Handle cases where no changes are made
    old_email = current_user.email
    new_email = params[:user][:email]
    new_pwd = params[:user][:password]
    if old_email == new_email && new_pwd == ''
      flash.notice = 'Account information not changed.'
      redirect_to after_update_path_for(current_user)
      return
    end
    super
  end

  private

  # Post-hook for successful signup
  # Called after a successful create
  def after_inactive_sign_up_path_for(resource)
    AdminMailer.user_added(resource).deliver_now
    flash.notice = 'Success! A confirmation email has been sent to you.'
    new_user_session_path
  end
end
