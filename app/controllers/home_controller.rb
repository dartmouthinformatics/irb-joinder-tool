# The main navigation and static content display Class.
# Home#Index is the root path for the application.
class HomeController < ApplicationController

  # This action is the navigation hub for the application.
  # Users are directed to the appropriate action based on:
  #   1. User.status
  #   2. User privileges
  #   3. User.institution.status
  def index
    # Non-Admins
    redir = fwa_start_path if current_user.status == 'new'
    redir = complete_profile_path if current_user.status == 'has_institution'
    if current_user.status == 'profile_complete'
      institution = current_user.institution
      redir = register_institution_path if institution.status == 'new'
      redir = offer_signatory_path if institution.status == 'profile_complete'
      redir = new_joinder_upload_path if institution.status == 'signatory_downloaded'
      redir = new_submission_path if institution.status == 'agreement_uploaded'
      if institution.status == 'submitted'
        submission = institution.submission
        redir = thank_you_path if submission.status == 'needs_review'
        redir = revise_path if submission.status == 'needs_revision'
        redir = approved_path if submission.status == 'approved'
        redir = rejected_path if submission.status == 'rejected'
      end
    end
    # Admins
    redir = admin_path if current_user.admin?
    redirect_to redir
  end

  # Informational page displayed after a submission.
  def thank_you
    unless confirm_submission_status('needs_review')
      redirect_to :root
      return
    end
    @institution = current_user.institution
  end

  # Informational page displayed when a submission needs revision.
  def revise
    unless confirm_submission_status('needs_revision')
      redirect_to :root
      return
    end
    submission = current_user.institution.submission
    @notes = submission.last_status_note
  end

  # Informational page displayed when a submission has been approved.
  def approved
    unless confirm_submission_status('approved')
      redirect_to :root
      return
    end
    submission = current_user.institution.submission
    @notes = submission.last_status_note
  end

  # Informational page displayed when a submission has been rejected.
  def rejected
    redirect_to :root unless confirm_submission_status('rejected')
    submission = current_user.institution.submission
    @notes = submission.last_status_note
  end

end
