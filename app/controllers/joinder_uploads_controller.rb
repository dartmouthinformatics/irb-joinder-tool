# This class handles the creation of joinder document uploads
class JoinderUploadsController < ApplicationController

  # Lists existing joinder uploads for the current User's institution.
  def index
    institution = current_user.institution
    @submission = institution.submission
    @uploads = institution.uploads.order('created_at DESC').all
  end

  # Creates a new Upload object and renders the upload form.
  def new
    @upload = Upload.new
  end

  # Performs an upload and saves the file
  def create
    @upload_params = upload_params
    @upload_params[:user_id] = current_user.id
    @upload = Upload.new(@upload_params)
    if @upload.save
      redirect_to joinder_uploads_path
    else
      # Delete redundant error
      @upload.errors.delete :doc
      flash.now.alert = @upload.errors.full_messages.join("<br>").html_safe
      render :new
    end
  end

  # If authorized, allow the current user to download the Upload identified by the passed-in ID
  def download
    begin
      upload = Upload.find(params[:id])
    rescue
      flash.now.alert = 'Could not find a file with that ID'
      redirect_to root_url
      return
    end
    file_with_path = "#{Rails.root}/uploads/#{upload.id}/#{upload.doc_file_name}"
    if upload.user == current_user || current_user.privileged?
      send_file file_with_path, disposition: 'inline' if upload.user.institution == current_user.institution || current_user.privileged?
    else
      flash.now.alert = 'Not Authorized to view this file'
      redirect_to root_url
    end
  end

  private

  # Whitelist of parameters for creating an upload.
  def upload_params
    params.require(:upload).permit(:doc, :kind)
  end

end
