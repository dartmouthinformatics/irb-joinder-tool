class StudiesController < ApplicationController

  before_filter :only_admins, except: [:index]

  # List existing studies
  def index
    if current_user.privileged?
      @studies = Study.all.order('created_at DESC')
    else
      @studies = current_user.institution.studies.order('created_at DESC')
    end
  end

  # Instantiate a new study
  def new
    @study = Study.new
  end

  # Save a new study
  def create
    @study = Study.new(study_params)
    if @study.save
      # NOTE: Create a root study folder every time a study is created
      folder = Folder.new
      folder.name = @study.title
      folder.study_id = @study.id
      folder.save
      redirect_to new_site_path(study_id: @study.id)
    else
      flash.now.alert = @study.errors.full_messages.join("<br>").html_safe
      render :new
    end
  end

  # Save changes to a study
  def update
    @study = Study.find(study_params[:id])
    if @study.update_attributes(study_params)
      redirect_to edit_study_path(id: @study.id)
    else
      flash.now.alert = @study.errors.full_messages.join("<br>").html_safe
      # NOTE: This needs to be more sophisiticated if updates other than title are added.
      render :edit_title
    end
  end

  # Render study edit view (includes new site form)
  def edit
    @study = Study.find(params[:id])
    @institution_names = Institution.approved.order(:name).map {|i| i.name}
    @study.sites.each do |site|
      @institution_names.delete(site.institution.name)
    end
    redirect_to new_site_path(study_id: params[:id]) unless @study.reviewing_site
  end

  # Edit study title
  def edit_title
    @study = Study.find(params[:id])
  end

  private

  # Params whitelist
  def study_params
    params.require(:study).permit(:id, :title)
  end

end
