class SitesController < ApplicationController

  before_filter :only_admins

  def index
  end

  def new
    @study = Study.find(params[:study_id])
    @site = Site.new
    @institution_names = Institution.approved.order(:name).map {|i| i.name}
    if @study.reviewing_site
      @sites = @study.sites
      # Remove existing site names from elibigle new site names
      @sites.each do |site|
        @institution_names.delete(site.institution.name)
      end
      render :new
    else
      render :new_reviewing
    end
  end

  def create
    @site_params = site_params
    @site_params[:institution] = Institution.find_by(name: @site_params[:institution])
    @site = Site.new(@site_params)
    if @site.save
      # NOTE: Create a root site folder every time a site is created
      folder = Folder.new
      folder.name = @site.institution.name
      folder.site_id = @site.id
      folder.save
      redirect_to :back
    else
      flash.alert = @site.errors.full_messages.join("<br>").html_safe
      redirect_to new_site_path(study_id: @site.study_id)
    end
  end

  def edit
    @site = Site.find(params[:id])
    @study = @site.study
    @institution_names = Institution.approved.order(:name).map {|i| i.name}
    @study.sites.each do |site|
      unless site.id == @site.id
        @institution_names.delete(site.institution.name)
      end
    end
  end

  def update
    @site_params = site_params
    @site = Site.find(@site_params[:id])
    @study = Study.find(@site.study_id)
    @site_params[:institution] = Institution.find_by(name: @site_params[:institution])
    unless @site_params[:institution]
      flash.alert = 'Could not find an approved institution with that name.'
      redirect_to edit_site_path(id: @site_params[:id])
      return
    end
    if @site.update_attributes(@site_params)
      redirect_to edit_study_path(id: @study.id)
    else
      flash.alert = @site.errors.full_messages.join("<br>").html_safe
      redirect_to edit_site_path(id: @site_params[:id])
    end
  end

  def destroy
    site = Site.find(params[:id]).destroy
    flash.notice = 'Site deleted'
    redirect_to edit_study_path(id: site.study_id)
  end

  private

  def site_params
    params.require(:site).permit(:id, :study_id, :institution, :reviewing)
  end

end
