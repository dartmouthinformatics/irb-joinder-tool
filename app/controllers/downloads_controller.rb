# Mangages downloads of dynamically generated PDF files. These PDF files 
# are the signatory documents that are the joinder agreement for a given institution.
class DownloadsController < ApplicationController

  # Offers link to download_signatory
  def offer_signatory
    @institution = current_user.institution
  end

  # Called via JS in notify
  # Uses the index view as the template for the PDF download
  def download_signatory
    @institution = current_user.institution
    # Allows updating the insitution status
    @institution.update_attributes(
      signatory_last_downloaded_by: current_user.id,
      signatory_last_downloaded_at: Time.now
    )
    html = render_to_string(action: :index, layout: false)
    pdf = PDFKit.new(html)
    send_data(pdf.to_pdf, filename: 'Joinder_Agreement.pdf')
  end

end
