# This class contains methods for managing Downloaders
# NOTE: This controller is designed to be publically accessible and 
# its views are intended to be embedded on an external site.
class DownloadersController < ApplicationController
  skip_before_action :authenticate_user!, except: [:delete]
  before_action :only_admins, only: [:delete]
  after_action :allow_iframe

  # Layout has no top navbar - for embedding in informational website
  layout 'downloaders'

  # Instantiate a new Downloader
  def new
    @downloader = Downloader.new
  end

  # Save a new Downloader
  def create
    @downloader = Downloader.where(downloader_params).first_or_create
    @downloader.token = SecureRandom.hex(10)
    if verify_recaptcha(model: @downloader) && @downloader.save
      redirect_to downloader_path(@downloader.id)
    else
      flash.now.alert = @downloader.errors.full_messages.join("<br>").html_safe
      render :new
    end
  end

  # Show downloader. Functions as thank you screen and context for JS
  # to trigger the actual download (see view).
  def show
    @downloader = Downloader.find(params[:id])
  end

  # Called via JS trigger in show view after create
  # Performs the actual download.
  def download
    downloader = Downloader.find(downloader_params[:id])
    downloader.update_attributes(downloaded_at: Time.now)
    send_file "#{Rails.root}/agreement/SMART_IRB_Agreement.pdf"
    AdminMailer.new_downloader(downloader).deliver_now
  end
  
  # Admins can "delete" a downloader (set deleted_at)
  def delete
    downloader = Downloader.find(downloader_params[:id])
    downloader.update_attributes(deleted_at: Time.now)
    flash.notice = 'Downloader deleted.'
    redirect_to :root
  end

  private

  # Params whitelist
  def downloader_params
    params.require(:downloader).permit(:id, :first_name, :last_name, :title, :email, :institution_name)
  end

  # Allows a specific website to embed content from this controller.
  def allow_iframe
    if Rails.env == 'production'
      response.headers['X-Frame-Options'] = 'ALLOW-FROM https://irbrely.org'
    else
      response.headers['X-Frame-Options'] = 'ALLOW-FROM https://ic3d-dev.dartmouth.edu'
    end
  end

end
