# Methods for modifying and displaying the details of Institution records.
class InstitutionsController < ApplicationController
  skip_before_action :authenticate_user!, only: :json_institutions
  after_filter  :set_access_control_headers, only: :json_institutions

  #NOTE: Institutions are created in the UsersController after FWA selection.
  
  # From for completing the Institutional profile
  # Program logic prevents calling this method if current_user does
  # not have an institution.
  def register
    @institution = current_user.institution
  end

  # Destination from the Institution registration form
  # Admins also get here by updating institution information in the submissions tab
  def update
    @institution_params = institution_params
    @institution = Institution.find(@institution_params[:id])
    @institution.completing_profile = true if params[:commit] != 'Save Partial Form for Future Completion'
    if @institution.update_attributes(@institution_params)
      if @institution.completing_profile
        # Reset signatory_last_downloaded params every time in case this is a revision
        unless current_user.privileged?
          @institution.update_attributes(registering_user_id: current_user.id,
                                         registered_at: Time.now,
                                         signatory_last_downloaded_at: nil,
                                         signatory_last_downloaded_by: nil)
        end
        flash.notice = 'Institution registration successfully completed.'
      else
        if current_user.privileged?
          flash.notice = 'Institution registration updated.'
        else
          # Reset signatory_last_downloaded to nil so that user is taken to download page when profile is completed,
          # even if an older version of the agreement was already downloaded
          @institution.update_attributes(registering_user_id: nil,
                                         registered_at: nil,
                                         signatory_last_downloaded_at: nil,
                                         signatory_last_downloaded_by: nil)
          flash.notice = 'Institution profile saved for future completion.'
        end
      end
      redirect_to :root
    else
      if current_user.privileged?
        flash.alert = @institution.errors.full_messages.join("\n").html_safe
        redirect_to :root
      else
        flash.now.alert = @institution.errors.full_messages.join("\n").html_safe
        render :register
      end
    end
  end

  # Called from informational (external) website to build a directory
  # Returns a json object with all model attributes for each Institution that has not been rejected.
  def json_institutions
    institutions = []
    Institution.all.each do |institution|
      status = 'in process'
      status = 'rejected' if institution.submission && institution.submission.status == 'rejected'
      status = 'approved' if institution.submission && institution.submission.status == 'approved'
      unless status == 'rejected'
        institution_as_hash = {}
        institution_as_hash[:name] = institution.name
        institution_as_hash[:city] = institution.city
        institution_as_hash[:state] = institution.state
        institution_as_hash[:status] = status
        institution_as_hash[:poc_first_name] = institution.poc_first_name
        institution_as_hash[:poc_last_name] =  institution.poc_last_name
        institution_as_hash[:poc_email] = institution.poc_email
        institution_as_hash[:altpoc_first_name] = institution.altpoc_first_name
        institution_as_hash[:altpoc_last_name] =  institution.altpoc_last_name
        institution_as_hash[:altpoc_email] = institution.altpoc_email
        institution_as_hash[:io_first_name] = institution.io_first_name
        institution_as_hash[:io_last_name] =  institution.io_last_name
        institution_as_hash[:io_email] = institution.io_email
        institutions << institution_as_hash
      end
    end
    render json: institutions
  end

  # Called via AJAX
  # Returns a JSON object of approved institution names for use in typeahead search
  def names
    render json: Institution.approved.order(:name).map {|i| i.name}
  end

  private

  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
  end   

  # Whitelisted parameters for updating and Institution.
  def institution_params
    params.require(:institution).permit(
      :id,
      :name,
      :street,
      :city,
      :state,
      :zip,
      :university,
      :academic_medical_center,
      :community_hospital,
      :cancer_center,
      :other_type,
      :ctsa_funded,
      :fwa_box_unchecked,
      :hrpp_accredited,
      :accrediting_organization,
      :accredited_at,
      :ohrp,
      :ohrp_completed_at,
      :poc_first_name,
      :poc_last_name,
      :poc_email,
      :poc_title,
      :poc_phone,
      :altpoc_first_name,
      :altpoc_last_name,
      :altpoc_email,
      :altpoc_title,
      :altpoc_phone,
      :io_first_name,
      :io_last_name,
      :io_email,
      :io_title,
      :io_phone,
      :ctsa,
      :hrpp_pursuing_accreditation,
      :pursued_accrediting_organization,
      :pursued_accreditation_status,
      :ohrp_status,
      :hrpp_other,
      :hrpp_other_description,
      :no_irb,
      :poc_street,
      :poc_city,
      :poc_state,
      :poc_zip,
      :altpoc_street,
      :altpoc_city,
      :altpoc_state,
      :altpoc_zip,
      :io_street,
      :io_city,
      :io_state,
      :io_zip
    )
  end
  
end
