# Admin-only actions live here
class AdminController < ApplicationController

  before_action :only_admins

  # The main admin interface, including all tabs
  def index
    # no-op
  end

  # Linked to from forms on the Downloaders tab.
  # Updates the Downloader as having been invited
  # and sends invitation email.
  def invite
    downloader = Downloader.find(params[:id])
    downloader.update_attributes(invited_at: Time.now)
    UserMailer.invite(downloader.email, downloader.token).deliver_now
    flash.notice = "An invitation email was sent to #{downloader.email}"
    redirect_to :root
  end

end
