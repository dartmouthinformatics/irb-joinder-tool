class FoldersController < ApplicationController

  before_action :check_read_access, only: [:show]
  before_action :check_write_access, except: [:show]

  # Create a new folder
  def create
    folder = Folder.new(folder_params)
    if folder.save
      # no-op
    else
      flash.alert = folder.errors.full_messages.join("<br>").html_safe
    end
    redirect_to :back
  end

  # Main document management interface
  def show
    @folder = Folder.find(params[:id])
    @writeable = current_user.can_write_to_folder?(@folder)
  end

  # Only used for renaming folders as of now
  def update
    @folder_params = folder_params
    @folder = Folder.find(@folder_params[:id])
    if @folder.update_attributes(@folder_params)
      # no-op
    else
      flash.alert = @folder.errors.full_messages.join("<br>").html_safe
    end
    # Rename is always done from within a parent folder
    redirect_to folder_path(@folder.parent.id)
  end

  # Destroys a folder and all of its children, folders and uploads
  def destroy
    folder = Folder.find(params[:id])
    folder.destroy
    folder.all_child_uploads.each {|u| u.destroy}
    folder.all_child_folders.each {|f| f.destroy}
    # Delete is always done from within a parent folder
    redirect_to folder_path(folder.parent.id)
  end

  private

  # Ensures that the current user has read access to the current folder
  def check_read_access
    redirect_to :back unless current_user.can_read_folder?(Folder.find(params[:id]))
  end

  # Ensures that the current user has write access to the current folder
  def check_write_access
    id = params[:id] || folder_params[:id] || folder_params[:parent_id]
    redirect_to :back unless current_user.can_write_to_folder?(Folder.find(id))
  end

  # Params whitelist
  def folder_params
    params.require(:folder).permit(:id, :name, :study_id, :site_id, :parent_id, :is_admin)
  end

end
