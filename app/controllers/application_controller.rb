# Methods in this Class are available to all controllers.
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks
  protect_from_forgery

  # Authenticate for all actions by default
  before_action :authenticate_user!
  # Prevent browser caching for post-logout security
  before_action :set_cache_buster
  # Reset session before certain actions
  before_action :check_session_reset

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def check_session_reset
    controller = params[:controller]
    action = params[:action]
    reset_session if controller == 'devise/confirmations' && action == 'show'
  end

  # Only allow admins and superadmins to access action.
  # Used in a before_filter in individual controllers for authorization.
  def only_admins
    redirect_to root_url unless current_user and current_user.privileged?
  end

  # Checks submission status for the current user's institution
  def confirm_submission_status(status)
    if current_user
      institution = current_user.institution
      if institution
        submission = institution.submission
        if submission
          return true if submission.status == status
        end
      end
    end
    return false
  end

  def after_sign_in_path_for(resource)
    # Handle sign_in redirects when last page was a send_file page
    if [download_agreement_path, download_signatory_path].include?(stored_location_for(resource))
      root_path
    else
       super
    end
  end

end
