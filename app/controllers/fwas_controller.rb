# Methods for linking FWAs to users
# NOTE: FWAs are not created within the application. They are created by an external script.
# See the FWA model for more information.
# NOTE: Institution records are created here, when a user selects a previously unselected FWA.
class FwasController < ApplicationController

  skip_before_action :authenticate_user!, only: :names
  before_action :check_user_status, except: :names

  # GET fwa_start_path
  # Introductory text and a link to FWA search
  def start
    # no-op
  end

  # GET fwa_search_path
  # Initiate an FWA search
  def search
    render :search_form
  end

  # POST fwa_search_results_path
  # Display results of an FWA search.
  def search_results
    @fwa_params = params[:fwa]
    if @fwa_params[:name].blank? && @fwa_params[:num].blank? && @fwa_params[:city].blank?
      flash.now.alert = 'Please fill out at least one field.'
      render :search_form
    else
      @fwas = Fwa.search(@fwa_params)
      render :no_results if @fwas.length == 0
    end
  end

  # GET fwa_select_path
  # expects an ID param for an FWA.
  # Gives user the chance to associate with that FWA.
  def select
    @fwa = Fwa.find(params[:id])
  end

  # Called via AJAX
  # Returns a JSON object of FWA names for use in typeahead search
  def names
    render json: Fwa.active.primary.order(:name).map {|fwa| fwa.name}
  end

  private

  # Users should not have an Institution when they hit these actions.
  # Admins should also be redirected to home (they don't belong in this controller)
  # Redirect if they do.
  def check_user_status
    redirect_to root_url if current_user.institution || current_user.privileged?
  end

end
