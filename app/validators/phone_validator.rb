# http://stackoverflow.com/a/20971688/1038184
class PhoneValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value.length >= 10 && value =~ /\A(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?\z/i
      record.errors.add(attribute, options[:message] || :phone)
    end
  end
end