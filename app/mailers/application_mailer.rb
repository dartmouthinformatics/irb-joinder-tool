# Sets defaults for all mailer controllers
class ApplicationMailer < ActionMailer::Base
  default from: "no-reply@smartirb.org"
  layout 'mailer'
end
