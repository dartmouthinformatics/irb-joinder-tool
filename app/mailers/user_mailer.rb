# Handles sending email to regular users, and invitation emails do Downloaders.
class UserMailer < ApplicationMailer

  def submission_status_changed(submission, email_body, user)
    @custom_text = email_body
    @institution = submission.institution
    r1 = @institution.poc_email
    r2 = @institution.altpoc_email
    r3 = @institution.io_email
    r4 = submission.submitter.email
    cc = User.admins.map {|u| u.email}
    subject = "SMART IRB Joinder Activation: #{@institution.name}" if submission.status == 'approved'
    subject = "SMART IRB Joinder Submission Update: Unable to activate #{@institution.name}" if submission.status == 'rejected'
    subject = "SMART IRB Joinder Submission Needs Revision: #{@institution.name}" if submission.status == 'needs_revision'
    bcc = 'DoNotReplyJoinder@smartirb.org'
    mail(to: [r1, r2, r3, r4], cc: cc, subject: subject, bcc: bcc) do |format|
      # Mail template name is the same as the submission status string
      format.html { render submission.status }
    end
  end

  def invite(to, token)
    @token = token
    bcc = 'DoNotReplyJoinder@smartirb.org'
    mail(to: to, bcc: bcc, subject: 'Your invitation to join SMART IRB')
  end

  def contact(from, subject, email_body)
    subject = 'Question re: SMART IRB Joinder System: ' + subject
    to = User.admins.map {|u| u.email}
    bcc = 'DoNotReplyJoinder@smartirb.org'
    @email_body = email_body
    mail(from: from, to: to, subject: subject, bcc: bcc)
  end

  def new_submission(submitting_user)
    @institution_name = submitting_user.institution.name
    subject = "SMART IRB Joinder Submission submitted: #{@institution_name}"
    to = submitting_user.email
    bcc = 'DoNotReplyJoinder@smartirb.org'
    mail(to: to, subject: subject, bcc: bcc)
  end
	
	def password_changed(user)
		@user = user
    subject = 'SMART IRB password changed'
    bcc = 'DoNotReplyJoinder@smartirb.org'
		mail(to: @user.email, subject: subject, bcc: bcc)
  end

end
