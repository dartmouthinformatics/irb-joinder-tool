# Handles sending mail to admins
class AdminMailer < ApplicationMailer

  def user_added(user)
    @user_email = user.email
    to = User.admins.map {|u| u.email}
    mail(to: to, subject: 'New SMART IRB user registration') if to.first
  end

  def new_downloader(downloader)
    @downloader = downloader
    to = User.admins.map {|u| u.email}
    bcc = 'DoNotReplyJoinder@smartirb.org'
    mail(to: to, subject: 'New SMART IRB agreement download', bcc: bcc) if to.first
  end

  def new_submission(submitting_user)
    @user = submitting_user
    @institution_name = submitting_user.institution.name
    subject = "New Joinder Submission Received: #{@institution_name}"
    to = User.admins.map {|u| u.email}
    mail(to: to, subject: subject, bcc: 'DoNotReplyJoinder@smartirb.org')
  end

end
