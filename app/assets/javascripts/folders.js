// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(function() {

  // Upload context menu
  $.contextMenu({
    selector: '.upload-context-menu',
    callback: function(key, options){
      var elementID = options.$trigger[0].id;
      if(key == 'rename'){
        renameUpload(elementID);
      }
      if(key == 'delete'){
        deleteUpload(elementID);
      }
    },
    items: {
             'rename': {name: 'Rename'},
             'delete': {name: 'Delete'}
           }
  });

  // Folder context menu
  $.contextMenu({
    selector: '.folder-context-menu',
    callback: function(key, options){
      var elementID = options.$trigger[0].id;
      if(key == 'rename'){
        renameFolder(elementID);
      }
      if(key == 'delete'){
        deleteFolder(elementID);
      }
    },
    items: {
             'rename': {name: 'Rename'},
             'delete': {name: 'Delete'}
           }
  });

  // Closes rename upload form
  $('.cancel-upload-rename').click( function(event){
    var id = $(event.target).attr('id');
    $('.upload-rename-form#' + id).hide();
    $('.upload-link#' + id).show();
  });

  // Closes rename folder form
  $('.cancel-folder-rename').click( function(event){
    var id = $(event.target).attr('id');
    $('.folder-rename-form#' + id).hide();
    $('.folder-link#' + id).show();
  });

  // Opens new folder dialog
  $('.new-folder').click( function() {
    $('#new-folder-form').slideDown(200);
    $('#new-folder-name').focus();
  });

  // Closes new folder dialog
  $('#cancel-new-folder').click( function() {
    $('#new-folder-form').slideUp(200);
  });

  // Opens new upload dialog
  $('.new-upload').click( function() {
    $('#new-upload-form').slideDown(200);
  });

  // Closes new upload dialog
  $('#cancel-new-upload').click( function() {
    $('#new-upload-form').slideUp(200);
  });

});

function renameFolder(elementID){
  $('.folder-rename-form#' + elementID).show();
  $('.folder-link#' + elementID).hide();
  $('.folder-rename-text-field#' + elementID).focus().select();
}

function deleteFolder(elementID){
  $('.folder-delete-link#' + elementID).click();
}

function renameUpload(elementID){
  $('.upload-rename-form#' + elementID).show();
  $('.upload-link#' + elementID).hide();
  $('.upload-rename-text-field#' + elementID).focus().select();
}

function deleteUpload(elementID){
  $('.upload-delete-link#' + elementID).click();
}
