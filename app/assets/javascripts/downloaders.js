// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

var recaptchaChecked = false;

function validateEmail(email){
  var re = /\S+@\S+\.\S+/;
  return re.test(email);
}

function requiredFields(){
  var institution = $('#downloader_institution_name').val();
  var first_name = $('#downloader_first_name').val();
  var last_name = $('#downloader_last_name').val();
  var title = $('#downloader_title').val();
  return !!institution && !!first_name && !!last_name && !!title;;
}

// Will be > 0 when recaptcha has been checked, 0 otherwise
function recaptchaChecked(){
	return grecaptcha.getResponse().length > 0;
}

function setRecaptchaChecked(){
  recaptchaChecked = true;
  validateFieldsAndRecaptchaChecked();
}

// Enables the download button when:
//   - A "valid" email has been entered
//   - There is text in all other fields
//   - The recaptcha has been checked
function validateFieldsAndRecaptchaChecked(){
  var email = $('#downloader_email').val();
  if (validateEmail(email) && requiredFields() && recaptchaChecked) {
    $('#downloader-submit').attr('disabled', false);
  } else {
    $('#downloader-submit').attr('disabled', 'disabled');
  }
}

$(document).ready(function () {

	// Check for complete and valid entries on every keyup within form
  $('.downloader-validate').on('keyup blur', function () {
    validateFieldsAndRecaptchaChecked();
  });

	// Add callback to recaptcha element
	$('.g-recaptcha:first').attr('data-callback', 'setRecaptchaChecked');

});   
