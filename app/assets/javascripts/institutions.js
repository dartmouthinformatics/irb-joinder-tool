// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function(){

  // Register datepickers (bootstrap-datepicker-rails)
  $('.datepicker').datepicker({
    format: "MM d, yyyy",
    endDate: '+0d',
    autoclose: true
  });

  // Clear datepickers when they are clicked on
  $('.datepicker').click(function(){
    $(this).val('');
  })

  // CTSA institution selection visibility
  var CtsaSelect = $('#ctsa-select');
  $('#institution_ctsa_funded_true').click( function(){
    CtsaSelect.slideDown();
  })
  $('#institution_ctsa_funded_false').click( function(){
    CtsaSelect.slideUp();
    // Make sure no CTSA is selected
    $('select#institution_ctsa option').prop('selected', false);
  })

  // hrpp_accredited:
    // Set initial visibility state for already-filled-out form
    if($('#institution_hrpp_accredited').prop('checked')){
      $('#accrediting-organization').show();
    }
    // Show hide details when checkbox clicked
    $('#institution_hrpp_accredited').click( function(event){
      var showHide = $('#accrediting-organization');
      var checked = $(event.target).prop('checked');
      if(checked){
        showHide.slideDown();
      }
      else {
        showHide.slideUp();
      }
    });

  // hrpp_pursuing_accreditation:
    // Set initial visibility state for already-filled-out form
    if($('#institution_hrpp_pursuing_accreditation').prop('checked')){
      $('#pursued-accrediting-organization').show();
    }
    // Show hide details when checkbox clicked
    $('#institution_hrpp_pursuing_accreditation').click( function(event){
      var showHide = $('#pursued-accrediting-organization');
      var checked = $(event.target).prop('checked');
      if(checked){
        showHide.slideDown();
      }
      else {
        showHide.slideUp();
      }
    });

  // ohrp:
    // Set initial visibility state for already-filled-out form
    if($('#institution_ohrp').prop('checked')){
      $('#ohrp-details').show();
    }
    // Show hide details when checkbox clicked
    $('#institution_ohrp').click( function(event){
      var showHide = $('#ohrp-details');
      var checked = $(event.target).prop('checked');
      if(checked){
        showHide.slideDown();
      }
      else {
        showHide.slideUp();
      }
    });

  // hrpp_other:
    // Set initial visibility state for already-filled-out form
    if($('#institution_hrpp_other').prop('checked')){
      $('#hrpp-other-description').show();
    }
    // Show hide details when checkbox clicked
    $('#institution_hrpp_other').click( function(event){
      var showHide = $('#hrpp-other-description');
      var checked = $(event.target).prop('checked');
      if(checked){
        showHide.slideDown();
      }
      else {
        showHide.slideUp();
      }
    });

});
