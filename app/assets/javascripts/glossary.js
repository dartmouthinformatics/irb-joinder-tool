$(function() {
  $('.glossary-fwa').attr('title', "The Federalwide Assurance agreement between a research institution and OHRP, stipulating terms by which the institution will protect the safety, welfare and rights of research participants in accordance with the Common Rule and other applicable federal regulations.")
  $('.glossary-hrpp').attr('title', "Human Research Protection Program.")
  $('.glossary-io').attr('title', "The person who has the authority on behalf of an institution to bind such institution to the terms and conditions of this Agreement.")
  $('.glossary-irb').attr('title', "Institutional Review Board.")
  $('.glossary-joinder').attr('title', "Agreement, the form of which is set forth in Exhibit B of the National IRB Reliance Agreement, by which an institution may become a Participating Institution and bound by the terms and conditions of this Agreement.")
  $('.glossary-poc').attr('title', "The person designated at each network site to make determinations regarding requests for his/her site to serve as the Reviewing IRB for a study or cede IRB review to an external IRB. POCs are likely to be individuals within an IRB office. Regulatory POCs are appointed at both the Reviewing IRB's institution and Relying Sites/Institutions.")
});
