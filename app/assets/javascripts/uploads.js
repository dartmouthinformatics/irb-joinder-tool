// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(function() {
  $('#file-select').change(function() {
    if($('#file-select').val() == ''){
      console.log('hiding submit');
      $('#file-submit').hide();
      $('#view-existing').show();
    } else {
      console.log('showing submit');
      $('#file-submit').show();
      $('#view-existing').hide();
    }
  });
});
