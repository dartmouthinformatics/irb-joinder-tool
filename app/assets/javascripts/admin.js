// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function () {

  // Initialize tablesorter
  $('.sorted-table').tablesorter({
    theme: 'default'
  });

  // Hide and show rows in the admin submssions table based on checkbox state
  $('.submission_type_check_box').click( function(){
    // Naming convention: The ID of the checkbox is the same as the class of
    // the row that is to be shown or hidden.
    if (this.checked){
      $('.' + this.id).show();
    }
    else{
      $('.' + this.id).hide();
    }
  })

  // Show the submisssion status change email field only when the submission
  // status is actually changed.
  $("input[name='submission[status]']").change(function(){
    $(this).parent().siblings('.status-change-email').fadeIn();
  });

  // Store the last tab visited in localStorage.
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      localStorage.setItem('lastTab', $(this).attr('href'));
  });

  // Go to the last tab  visited, if any.
  var lastTab = localStorage.getItem('lastTab');
  if (lastTab) {
      $('[href="' + lastTab + '"]').tab('show');
  }

  // Content is initally hidden (via layout) until page is loaded
  $('#admin-content').show();

  // Using a link to submit the delete downloader form
  $('.submit-downloader-delete').click(function(e){
    e.preventDefault();
    if(confirm('Are you sure you want to delete this downloader?')){
      $(this).closest('form').submit();
    }
  });

});   
