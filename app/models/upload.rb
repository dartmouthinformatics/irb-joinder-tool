# An upload is an uploaded file that is the signed, scanned Joinder agreement.
# Uploads belong to and Institution through the Users table.
# When a submission is created, the most recent Upload for the given Institution
# is associated with that Submission.
class Upload < ActiveRecord::Base
  
  belongs_to :folder
  belongs_to :user
  has_one :submission

  # https://github.com/thoughtbot/paperclip
  # Saving to a non-default (non-public) path for security
  # This path must be linked as shared in Capistrano deployments
  has_attached_file :doc,
    path: ":rails_root/uploads/:id/:filename",
    url:  "/uploads/:id/:basename.:extension"

  validates :kind, presence: true
  validates :doc, attachment_presence: true
  validates_attachment_file_name :doc, matches: [/pdf\Z/i, /png\Z/i, /jpe?g\Z/i, /gif\Z/i], message: 'is an invalid type.'
  validates_with AttachmentSizeValidator, attributes: :doc, less_than: 10.megabytes

  scope :joinder, -> { where('kind = ?', 'joinder') }

end
