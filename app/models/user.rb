# This User model includes regular Users as well as admins.
# Devise is used for registration, authentication, and confirmation of Users.
# User.status determines what is visible by a regular user during the Joinder process.
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :timeoutable, timeout_in: 30.minutes

  belongs_to :institution
  belongs_to :role
  has_many :uploads
  has_one :submission, through: :institution

  before_save :set_default_role
  after_update :send_password_change_email, if: :needs_password_change_email?

	validates :phone, phone: true, if: '!phone.blank?'
  validates :first_name, :last_name, :phone, :title, :degrees, presence: true, if: :completing_profile
  validates_format_of :first_name, with: /\A[a-zA-Z  \.]+\Z/, if: 'completing_profile && !first_name.blank?'
  validates_format_of :last_name, with: /\A[a-zA-Z \.]+\Z/, if: 'completing_profile && !last_name.blank?'
  validates_format_of :title, with: /\A[a-zA-Z \.]+\Z/, if: 'completing_profile && !title.blank?'

  scope :admins, -> { joins(:role).where('name = ?', 'admin') }

	# Override Devise's method of the same name, respecting db active value (defaults to true)
	def active_for_authentication?
		super && active
	end

  # Only require email confirmation for non-invited users
  def confirmation_required?
    return false if Downloader.find_by(email: email)
    true
  end

  # This boolean is only set to true for the profile completion form
  attr_accessor :completing_profile

  # Default this attribute to false
  def completing_profile
    @completing_profile || false
  end

  # Status is used to direct users through the proces of selecting an institution
  # and completing their profile. See Home#Index.
  def status
    # Handles not-yet-saved users
    return nil unless self.role
    # Admins and superadmins have no status
    return nil if self.admin? || self.superadmin?
    status = 'new'
    status = 'has_institution' if self.institution
    status = 'profile_complete' if profile_complete?
    status
  end

  # Is the current user an admin?
  def admin?
    role.try(:name) == 'admin'
  end

  # Is the current user a superadmin?
  def superadmin?
    role.try(:name) == 'superadmin'
  end

  # Is the current user either an admin or a superadmin?
  def privileged?
    admin? || superadmin?
  end

  # Static list of degrees that is presented on the institutional registration form
  def self.available_degrees
    ['BA', 'DDS', 'MD', 'BS', 'DMD', 'OD', 'MA', 'DNP', 'PharmD', 'MBA', 'DrPH', 'PhD', 'MPH', 'DVM', 'ScD', 'MS', 'JD', 'Other']
  end

  # Is self's profile complete? Used to determine self's status
  def profile_complete?
    self.valid? && institution_id && first_name && last_name && phone && title && degrees
  end

  # Is self associated with and institution that has an approved submission?
  def approved?
    institution && institution.approved?
  end

  # Returns a boolean indicating whether or not the passed-in folder can be read by self
  def can_read_folder?(folder)
    # Admins can read everything
    return true if privileged?
    # Sites in a study can read each other's folders (covers site-level folders)
    if folder.site
      folder.site.study.sites.each do |site|
        return true if site.institution == institution
      end
    end
    # Sites in a study can read each other's folders (covers study-level folders)
    if folder.study
      folder.study.sites.each do |site|
        return true if site.institution == institution
      end
    end
    # Recursively check parents for read access
    return can_read_folder?(folder.parent) if folder.parent
    false
  end

  # Returns a boolean indicating whether or not the passed-in folder can be written to by self
  def can_write_to_folder?(folder)
    # Admins can read everything
    return true if privileged?
    # Reviewing IRBs can write to everything within the study that they are reviewing
    study = folder.study if folder.study
    study = folder.site.study if folder.site
    if study && study.reviewing_site
      return true if study.reviewing_site.institution == institution
    end
    # Owners of site-level folders can write to them
    if folder.site
      return true if folder.site.institution == institution
    end
    # Recursively check parents for write access
    return can_write_to_folder?(folder.parent) if folder.parent
    false
  end

  private

  # Called before_save to make sure that all users have a role.
  def set_default_role
    self.role ||= Role.find_by_name('user')
  end

	# Returns true if the password has been changed for an existing user
	def needs_password_change_email?
    encrypted_password_changed? && persisted?
  end

  def send_password_change_email
    UserMailer.password_changed(self).deliver
  end

end
