# A Submission is an instance of and Institution submitting an Upload in order to complete the joinder process.
# Submissions are reviewed by admins. Admins change the status of a Submission based on this review.
class Submission < ActiveRecord::Base

  belongs_to :institution
  belongs_to :upload

  enum status: [:needs_review, :needs_revision, :approved, :rejected]

  validates :upload, :institution, :created_by, presence: true
  validates :notes, presence: true, allow_nil: true

  # User that created the Submission
  def submitter
    User.find(created_by)
  end

  # Boolean indicating whether or not self needs revision
  def needs_revision?
    self.status == 'needs_revision'
  end

  # Boolean indicating whether or not self has been approved
  def approved?
    self.status == 'approved'
  end

  # Late in the project, labels for submission status were changed.
  # Adding this hash was considered safer than changing the enum values.
  def status_label
    labels = {'needs_review' => 'under_review',
              'needs_revision' => 'needs_revision',
              'approved' => 'activated',
              'rejected' => 'not_activated'
             }
     labels[status]
  end

end
