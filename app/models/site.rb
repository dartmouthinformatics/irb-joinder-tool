class Site < ActiveRecord::Base
  belongs_to :study
  belongs_to :institution

  validates_uniqueness_of :institution_id, scope: :study_id
  validates :institution_id, presence: true

  def root_folder
    Folder.where(site_id: self.id).first
  end

end
