# An Institution is an organization (chosen from the FWA table) that has at least one registered user.
# Institutions are created when registered users select an FWA that has not been previously selected.
class Institution < ActiveRecord::Base
  # Associations
  has_many :users
  has_many :uploads, through: :users
  belongs_to :fwa
  has_one :submission
  has_many :sites
  has_many :studies, through: :sites

	enum fwa_box_unchecked: [:yes, :no_abcd, :no_a]

  # Validations
  # These validations must pass upon Institution creation (and always)
  validates :creating_user_id, :name, :fwa, :city, :state, presence: true

  # Phone numbers
  validates :poc_phone,    phone: true, if: '!poc_phone.blank?'
  validates :altpoc_phone, phone: true, if: '!altpoc_phone.blank?'
  validates :io_phone,     phone: true, if: '!io_phone.blank?'
  # Emails
  validates :poc_email,    email: true, if: '!poc_email.blank?'
  validates :altpoc_email, email: true, if: '!altpoc_email.blank?'
  validates :io_email,     email: true, if: '!io_email.blank?'
  # These validations must pass when submitting the instutional profile form for completion
  # Presence
  validates :poc_street, :poc_city, :poc_state, :poc_zip,
            :altpoc_street, :altpoc_city, :altpoc_state, :altpoc_zip,
            :io_street, :io_city, :io_state, :io_zip,
            :poc_first_name, :poc_last_name, :poc_title, :poc_email, :poc_phone,
            :altpoc_first_name, :altpoc_last_name, :altpoc_title, :altpoc_email, :altpoc_phone,
            :io_first_name, :io_last_name, :io_title, :io_email, :io_phone,
            :zip, :fwa_box_unchecked, presence: true, if: 'completing_profile'
  # Custom
  validates :ctsa_funded, :ohrp, inclusion: { in: [true, false], message: 'question was not answered' }, if: 'completing_profile'
  validates :ctsa, inclusion: { in: :ctsas, message: 'must be selected if CTSA funded'}, if: 'completing_profile && ctsa_funded'
  validates :accrediting_organization, :accredited_at, presence: true, if: 'completing_profile && hrpp_accredited'
  validates :pursued_accrediting_organization, :pursued_accreditation_status, presence: true, if: 'completing_profile && hrpp_pursuing_accreditation'
  validates :ohrp_completed_at, presence: true, if: 'completing_profile && ohrp && ohrp_status.blank?'
  validates :ohrp_completed_at, absence: {message: 'must be blank if Ohrp Status is not blank'}, if: 'completing_profile && ohrp && !ohrp_status.blank?'
  validates :ohrp_status, presence: true, if: 'completing_profile && ohrp && ohrp_completed_at.blank?'
  validates :ohrp_status, absence: {message: 'must be blank if Ohrp Completed At is not blank'}, if: 'completing_profile && ohrp && !ohrp_completed_at.blank?'
  validates :hrpp_other_description, presence: true, if: 'completing_profile && hrpp_other'
	validate :unique_poc_emails, if: 'completing_profile'
  validate :type_chosen, if: 'completing_profile'
  validate :hrpp_chosen, if: 'completing_profile'

  scope :approved, -> { joins(:submission).where('status = ?', 2) }

  # Status is used to expose/hide various institution-related pages
  def status
    # Handles not-yet-saved Institutions
    return nil unless self.id
    status = 'new'
    status = 'profile_complete' if profile_complete?
    status = 'signatory_downloaded' if signatory_downloaded? && profile_complete?
    status = 'agreement_uploaded' if agreement_uploaded? && signatory_downloaded? && profile_complete?
    status = 'submitted' if submitted?
    status
  end

  # This boolean is only set to true for the profile completion form
  # Used in conditional validation
  attr_accessor :completing_profile

  # Default this attribute to false
  def completing_profile
    @completing_profile || false
  end

  # Due to controller logic, an Institution will only have a registered_at value
  # if the profile was succesfully completed.
  def profile_complete?
    registered_at?
  end

  # Boolean indicating whether or not the signatory PDF has ever been downloaded
  # This attribute is updated in the Downloads controller
  def signatory_downloaded?
    signatory_last_downloaded_at?
  end

  # Boolean indicating whether or not this Insitution has any Uploads
  def agreement_uploaded?
    !!self.uploads.first
  end

  # Boolean indicating whether or not this Institution has a Submission
  def submitted?
    !!self.submission
  end

  # Boolean indicating whether or not self is approved
  def approved?
    submitted? && submission.status == 'approved'
  end

  # List of CSTA institutions from https://ctsacentral.org/consortium/institutions/
  def self.ctsas
    ["Albert Einstein College of Medicine",
    "Boston University",
    "Case Western Reserve University",
    "Children's National Medical Center",
    "Columbia University",
    "Dartmouth College",
    "Duke University",
    "Emory University",
    "Georgetown University with Howard University",
    "Harvard University",
    "Indiana University School of Medicine",
    "Johns Hopkins University",
    "Mayo Clinic",
    "Medical College of Wisconsin",
    "Medical University of South Carolina",
    "Mount Sinai School of Medicine",
    "New York University School of Medicine",
    "Northwestern University",
    "Ohio State University",
    "Oregon Health & Science University",
    "Penn State Milton S. Hershey Medical Center",
    "Rockefeller University",
    "Scripps Research Institute",
    "Stanford University",
    "Tufts University",
    "University at Buffalo",
    "University of Alabama at Birmingham",
    "University of Arkansas for Medical Sciences",
    "University of California Los Angeles",
    "University of California, Davis",
    "University of California, Irvine",
    "University of California, San Diego",
    "University of California, San Francisco",
    "University of Chicago",
    "University of Cincinnati",
    "University of Colorado Denver",
    "University of Florida",
    "University of Illinois at Chicago",
    "University of Iowa",
    "University of Kansas Medical Center",
    "University of Kentucky Research Foundations",
    "University of Massachusetts Medical School, Worcester",
    "University of Miami",
    "University of Michigan at Ann Arbor",
    "University of Minnesota Twin Cities",
    "University of New Mexico Health Sciences Center",
    "University of North Carolina at Chapel Hill",
    "University of Pennsylvania",
    "University of Pittsburgh",
    "University of Rochester School of Medicine and Dentistry",
    "University of Southern California",
    "University of Texas Health Science Center at Houston",
    "University of Texas Health Science Center at San Antonio",
    "University of Texas Medical Branch",
    "University of Texas Southwestern Medical Center at Dallas",
    "University of Utah",
    "University of Washington",
    "University of Wisconsin - Madison",
    "Vanderbilt University",
    "Virginia Commonwealth University",
    "Wake Forest Baptist Medical Center",
    "Washington University",
    "Weill Cornell Medical College",
    "Yale University"]
  end

  # This redundancy is needed because we can't access a class method from within a validation
  def ctsas
    self.class.ctsas
  end
  
  private

  # Validates that at least one institution type was chosen
  def type_chosen
    type_ok = university || academic_medical_center || community_hospital || cancer_center || other_type
    errors.add(:type, 'was not chosen. Please select at least one institution type.') unless type_ok
  end

  # Validates that at least one HRPP accreditation option was chosen
  def hrpp_chosen
    hrpp_ok = hrpp_accredited || hrpp_pursuing_accreditation || ohrp || hrpp_other || no_irb
    errors.add(:hrpp_accreditation_option, 'was not chosen. Please select at least one option.') unless hrpp_ok
  end

  # Validates that POC email and ALTPOC email are different
	def unique_poc_emails
		errors.add(:base, 'Alternate POC email must be different') if poc_email == altpoc_email
	end

end
