# A Downloader is a non-registered user who downloads the IRBrely agreement from an external public site
# Anyone on the Internet can be a Downloader. Some Downloaders are sent invitations to become Users by admin users.
class Downloader < ActiveRecord::Base

  validates :email, :first_name, :last_name, :title, :institution_name, presence: true, on: :create
  validates_format_of :email, :with => /\A[^@]+@([^@\.]+\.)+[^@\.]+\z/, if: '!self.email.blank?'
  validates_format_of :first_name, with: /\A[a-zA-Z  \.]+\Z/, if: '!first_name.blank?'
  validates_format_of :last_name, with: /\A[a-zA-Z \.]+\Z/, if: '!last_name.blank?'

  scope :not_deleted, -> { where(deleted_at: nil) }

end
