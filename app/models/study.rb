class Study < ActiveRecord::Base
  has_many :sites

  validates :title, presence: true

  def reviewing_site
    Site.where(study_id: self.id).where(reviewing: true).first
  end

  def relying_sites
    Site.where(study_id: self.id).where(reviewing: false).all
  end

  def root_folder
    Folder.where(study_id: self.id).first
  end

end
