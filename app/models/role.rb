# Roles differentiate admins from regular users.
# NOTE: Although a superadmin role is seeded, it is not currently used.
# TODO: Either implement a superadmin interface or remove the role.
class Role < ActiveRecord::Base
  has_many :users
end
