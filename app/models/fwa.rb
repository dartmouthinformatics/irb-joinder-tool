# An fwa is an organization that has an FWA number according to http://ohrp.cit.nih.gov/search/fwasearch.aspx?styp=bsc
# To populate the FWA table, searches were at that site were performed for each state, and the results were
# manually concatenated into one CSV file. That process must be repeated if the FWA table is to remain up-to-date
class Fwa < ActiveRecord::Base

  has_one :institution

  validates_uniqueness_of :num, scope: :name
  validates :num, :name, :city, :state, :kind, presence: true
  validates :active, inclusion: { in: [true, false] }

  scope :active, -> { where(active: true) }
  scope :primary, -> { where(kind: 'FWA') }
  scope :name_search, -> (name = nil) { where("lower(name) LIKE ?", "%#{name.downcase}%") }
  scope :city_search, -> (city = nil) { where("lower(city) LIKE ?", "%#{city.downcase}%") }
  scope :num_search, -> (num = nil) { where("num LIKE ?", "%#{num}%") }

  # Called from Fwas#search. Expects params to be a hash including 0 or more of:
  #   :name
  #   :city
  #   :num
  # and returns an ActiveRecord::Relation based on those params. If no params
  # are passed in, returns an empty array
  def self.search(params)
    # Return an empty array if no search parameters were passed in
    return [] if params[:name].blank? && params[:city].blank? && params[:num].blank?
    # Sart with active, primary FWAs
    fwas = Fwa.active.primary
    # Restrict the query for each non-empty search parameter
    fwas = fwas.name_search(params[:name]) unless params[:name].blank?
    fwas = fwas.city_search(params[:city]) unless params[:city].blank?
    fwas = fwas.num_search(params[:num]) unless params[:num].blank?
    fwas
  end

end
