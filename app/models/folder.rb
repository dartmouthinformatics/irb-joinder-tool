class Folder < ActiveRecord::Base

  extend ActsAsTree::TreeWalker
  acts_as_tree order: 'created_at DESC'

  validates :name, presence: true

  has_many :uploads
  belongs_to :site
  belongs_to :study

  # Return boolean indicating whether or not self is an admin folder
  # NOTE: This must be done differently if we add folder kinds other than study and admin
  def is_admin?
    !associated_study
  end

  # There should always be exactly 1 folder that meets these criteria
  # This folder is created by seeds.db
  def self.admin_root
    Folder.where(is_admin: true, parent_id: nil).first
  end

  # For any folder, return the associated study if there is one.
  def associated_study

    # For study root folders
    return self.study if self.study

    # For site root folders
    return self.site.study if self.site

    # For children of study folders or site folders
    folder = self.parent
    while folder
      return folder.study if folder.study
      return folder.site.study if folder.site
      folder = folder.parent
    end

    # For non-study-related folders (admin)
    return nil
  end

  # Return an array of all folders contained within self
  def all_child_folders
    folders = []
    self.walk_tree do |folder, level|
      folders << folder
    end
    folders
  end

  # Return an array of all uploads contained with in self and self's folders
  def all_child_uploads
    uploads = []
    self.uploads.each do |upload|
      uploads << upload
    end
    self.walk_tree do |folder, level|
      folder.uploads.each do |upload|
        uploads << upload
      end
    end
    uploads
  end

end
