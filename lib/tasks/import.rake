namespace :import do
  desc 'Imports FWA data from a | delimited file'
  task fwa: :environment do
    filename = Rails.root.to_s + '/lib/tasks/' + 'fwa_data_31-aug-15.txt'
    lines = File.readlines(filename)
    lines.each do |line|
      puts Fwa.active.count
      row = line.split('|')
      num = row[0]
      name = row[1]
      city = row[2].capitalize
      state = row[3]
      kind = row[4]
      active = row[5].chop == 'Active'
      if name.length > 0 
        Fwa.find_or_create_by(num: num,
                              name: name,
                              city: city,
                              state: state,
                              kind: kind,
                              active: active)
      end
    end
  end
end
