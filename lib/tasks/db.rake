namespace :db do

  desc "Dumps the database with a timestamp"
  task dump: :environment do
    app = 'irbrely'
    host = ActiveRecord::Base.connection_config[:host],
    db = ActiveRecord::Base.connection_config[:database],
    user = ActiveRecord::Base.connection_config[:username]
    cmd = "pg_dump --host #{host[0]} --username #{user} --verbose --clean --no-owner --no-acl --format=c #{db} > #{Rails.root}/backups/#{app}_#{Time.now.to_s.gsub(' ', '_')}.dump"
    puts cmd
    exec cmd
  end

  desc "Dumps the FWAs table"
  task dump_fwas: :environment do
    host = ActiveRecord::Base.connection_config[:host],
    db = ActiveRecord::Base.connection_config[:database],
    user = ActiveRecord::Base.connection_config[:username]
    cmd = "pg_dump --host #{host[0]} --username #{user} --verbose --no-owner --no-acl --format=c --table=fwas --data-only #{db} > #{Rails.root}/db/dumps/fwas.dump"
    puts cmd
    exec cmd
  end

  desc "Loads dumped FWAs table data into the db"
  task load_fwas: :environment do
    host = ActiveRecord::Base.connection_config[:host],
    db = ActiveRecord::Base.connection_config[:database],
    user = ActiveRecord::Base.connection_config[:username]
    cmd = "pg_restore --dbname=#{db} #{Rails.root}/db/dumps/fwas.dump"
    puts cmd
    exec cmd
  end

  desc "Cleans up name fields in FWAs table"
  task clean_fwa_names: :environment do
    Fwa.all.each do |fwa|
      name  = fwa.name

      # U -> Univsersity
      name = name.gsub(/^U /, 'University ').gsub(/ U$/, ' University').gsub(' U ', ' University ')
      # Ctr -> Center
      name = name.gsub(/^Ctr /, 'Center ').gsub(/ Ctr$/, ' Center').gsub(' Ctr ', ' Center ')
      # Hlth -> Health
      name = name.gsub(/^Hlth /, 'Health ').gsub(/ Hlth$/, ' Health').gsub(' Hlth ', ' Health ')
      # Hosp -> Hospital
      name = name.gsub(/^Hosp /, 'Hospital ').gsub(/ Hosp$/, ' Hospital').gsub(' Hosp ', ' Hospital ')
      # Rsch -> Research
      name = name.gsub(/^Rsch /, 'Research ').gsub(/ Rsch$/, ' Research').gsub(' Rsch ', ' Research ')
      # Med -> Medical
      name = name.gsub(/^Med /, 'Medical ').gsub(/ Med$/, ' Medical').gsub(' Med ', ' Medical ')
      # Cln -> Clinic
      name = name.gsub(/^Cln /, 'Clinic ').gsub(/ Cln$/, ' Clinic').gsub(' Cln ', ' Clinic ')
      # Rehab -> Rehabilitation
      name = name.gsub(/^Rehab /, 'Rehabilitation ').gsub(/ Rehab$/, ' Rehabilitation').gsub(' Rehab ', ' Rehabilitation ')
      # Inst -> Institute
      name = name.gsub(/^Inst /, 'Institute ').gsub(/ Inst$/, ' Institute').gsub(' Inst ', ' Institute ')

      fwa.update_attributes(name: name)
      puts fwa.name
    end
  end

end
