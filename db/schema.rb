# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160808175245) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "downloaders", force: :cascade do |t|
    t.string   "email"
    t.datetime "downloaded_at"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "institution_name"
    t.string   "title"
    t.datetime "invited_at"
    t.string   "token"
    t.datetime "token_used_at"
    t.datetime "deleted_at"
  end

  create_table "emails", force: :cascade do |t|
    t.string   "subject"
    t.string   "to"
    t.string   "from"
    t.string   "cc"
    t.string   "bcc"
    t.string   "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "folders", force: :cascade do |t|
    t.integer  "site_id"
    t.integer  "study_id"
    t.boolean  "is_admin"
    t.string   "name"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fwas", force: :cascade do |t|
    t.string   "num",        null: false
    t.string   "name",       null: false
    t.string   "city",       null: false
    t.string   "state",      null: false
    t.boolean  "active",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "kind"
  end

  create_table "institutions", force: :cascade do |t|
    t.integer  "creating_user_id",                 null: false
    t.string   "name",                             null: false
    t.integer  "fwa_id",                           null: false
    t.string   "street"
    t.string   "city",                             null: false
    t.string   "state",                            null: false
    t.string   "zip"
    t.boolean  "university"
    t.boolean  "academic_medical_center"
    t.boolean  "community_hospital"
    t.boolean  "cancer_center"
    t.boolean  "other_type"
    t.string   "other_type_description"
    t.boolean  "ctsa_funded"
    t.integer  "fwa_box_unchecked"
    t.boolean  "hrpp_accredited"
    t.string   "accrediting_organization"
    t.boolean  "ohrp"
    t.datetime "ohrp_completed_at"
    t.string   "poc_first_name"
    t.string   "poc_last_name"
    t.string   "poc_title"
    t.string   "poc_email"
    t.string   "poc_phone"
    t.string   "altpoc_first_name"
    t.string   "altpoc_last_name"
    t.string   "altpoc_title"
    t.string   "altpoc_email"
    t.string   "altpoc_phone"
    t.string   "io_first_name"
    t.string   "io_last_name"
    t.string   "io_title"
    t.string   "io_email"
    t.string   "io_phone"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.date     "accredited_at"
    t.integer  "registering_user_id"
    t.datetime "registered_at"
    t.datetime "signatory_last_downloaded_at"
    t.integer  "signatory_last_downloaded_by"
    t.string   "ctsa"
    t.boolean  "hrpp_pursuing_accreditation"
    t.string   "pursued_accrediting_organization"
    t.string   "pursued_accreditation_status"
    t.string   "ohrp_status"
    t.boolean  "hrpp_other"
    t.string   "hrpp_other_description"
    t.boolean  "no_irb"
    t.string   "poc_street"
    t.string   "poc_city"
    t.string   "poc_state"
    t.string   "poc_zip"
    t.string   "altpoc_street"
    t.string   "altpoc_city"
    t.string   "altpoc_state"
    t.string   "altpoc_zip"
    t.string   "io_street"
    t.string   "io_city"
    t.string   "io_state"
    t.string   "io_zip"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sites", force: :cascade do |t|
    t.integer  "institution_id", null: false
    t.integer  "study_id",       null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.boolean  "reviewing",      null: false
  end

  create_table "studies", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "submissions", force: :cascade do |t|
    t.integer  "approver_id"
    t.integer  "status"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "upload_id"
    t.integer  "institution_id",   null: false
    t.integer  "created_by",       null: false
    t.text     "notes"
    t.text     "last_status_note"
  end

  create_table "uploads", force: :cascade do |t|
    t.integer  "user_id",          null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "doc_file_name"
    t.string   "doc_content_type"
    t.integer  "doc_file_size"
    t.datetime "doc_updated_at"
    t.string   "kind"
    t.integer  "folder_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "role_id"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "institution_id"
    t.string   "phone"
    t.string   "title"
    t.string   "degrees"
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "active",                 default: true
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
