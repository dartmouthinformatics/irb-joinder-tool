class AddUploadIdToSubmissions < ActiveRecord::Migration
  def change
    add_column :submissions, :upload_id, :integer
  end
end
