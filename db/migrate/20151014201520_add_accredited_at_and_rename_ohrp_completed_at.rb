class AddAccreditedAtAndRenameOhrpCompletedAt < ActiveRecord::Migration
  def change
    add_column :institutions, :accredited_at, :date
    rename_column :institutions, :orhp_completed_at, :ohrp_completed_at
  end
end
