class ChangeFwaUncheckedToInteger < ActiveRecord::Migration
  def change
    change_column :institutions, :fwa_box_unchecked, 'integer USING CAST(fwa_box_unchecked AS integer)'
  end
end
