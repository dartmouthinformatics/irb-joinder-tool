class AddTokenToDownloaders < ActiveRecord::Migration
  def change
    add_column :downloaders, :token, :string
    add_column :downloaders, :token_used_at, :datetime
  end
end
