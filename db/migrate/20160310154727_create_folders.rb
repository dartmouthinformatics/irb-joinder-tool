class CreateFolders < ActiveRecord::Migration
  def change
    create_table :folders do |t|
      t.integer :site_id
      t.integer :study_id
      t.boolean :is_admin
      t.string :name
      t.integer :parent_id
      t.timestamps null: false
    end
  end
end
