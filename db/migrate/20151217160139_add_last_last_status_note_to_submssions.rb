class AddLastLastStatusNoteToSubmssions < ActiveRecord::Migration
  def change
    add_column :submissions, :last_status_note, :text
  end
end
