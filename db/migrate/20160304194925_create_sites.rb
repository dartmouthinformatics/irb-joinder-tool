class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.integer :institution_id, null: false
      t.integer :study_id, null: false
      t.timestamps null: false
    end
  end
end
