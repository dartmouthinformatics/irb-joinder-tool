class AddSignedJoinderToUploads < ActiveRecord::Migration
  def up
    add_attachment :uploads, :signed_joinder
  end

  def down
    remove_attachment :uploads, :signed_joinder
  end
end
