class FixFwaColumnName < ActiveRecord::Migration
  def change
    rename_column :fwas, :type, :kind
  end
end
