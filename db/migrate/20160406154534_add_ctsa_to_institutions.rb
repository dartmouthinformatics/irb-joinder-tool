class AddCtsaToInstitutions < ActiveRecord::Migration
  def change
    add_column :institutions, :ctsa, :string
  end
end
