class ChangeDownloaderFields < ActiveRecord::Migration
  def change
    remove_column :downloaders, :fwa_id
    add_column :downloaders, :institution_name, :string
  end
end
