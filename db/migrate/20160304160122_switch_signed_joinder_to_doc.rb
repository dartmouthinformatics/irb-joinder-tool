class SwitchSignedJoinderToDoc < ActiveRecord::Migration
  def up
    remove_attachment :uploads, :signed_joinder
    add_attachment :uploads, :doc
  end

  def down
    remove_attachment :uploads, :doc
    add_attachment :uploads, :signed_joinder
  end
end
