class AddDeletedAtToDownloaders < ActiveRecord::Migration
  def change
    add_column :downloaders, :deleted_at, :datetime 
  end
end
