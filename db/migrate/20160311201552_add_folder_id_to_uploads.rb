class AddFolderIdToUploads < ActiveRecord::Migration
  def change
    add_column :uploads, :folder_id, :integer
  end
end
