class CreateFwas < ActiveRecord::Migration
  def change
    create_table :fwas do |t|
      t.string  :num   , null: false
      t.string  :name  , null: false
      t.string  :city  , null: false
      t.string  :state , null: false
      t.boolean :active, null: false
      t.timestamps       null: false
    end
  end
end
