class AddInvitedAtToDownloaders < ActiveRecord::Migration
  def change
    add_column :downloaders, :invited_at, :datetime
  end
end
