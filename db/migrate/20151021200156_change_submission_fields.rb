class ChangeSubmissionFields < ActiveRecord::Migration
  def change
    remove_column :submissions, :user_id
    add_column :submissions, :institution_id, :integer, null: false
    add_column :submissions, :created_by, :integer, null: false
  end
end
