class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.integer :user_id, null: false
      t.integer :approver_id
      t.integer :status
      t.timestamps null: false
    end
  end
end
