class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.integer :creating_user_id, null: false
      t.string :name, null: false
      t.integer :fwa_id, null: false
      t.string :street
      t.string :city, null: false
      t.string :state, null: false
      t.string :zip
      t.boolean :university
      t.boolean :academic_medical_center
      t.boolean :community_hospital
      t.boolean :cancer_center
      t.boolean :other_type
      t.string :other_type_description
      t.boolean :ctsa_funded
      t.boolean :fwa_box_unchecked
      t.boolean :hrpp_accredited
      t.string :accrediting_organization
      t.boolean :ohrp
      t.datetime :orhp_completed_at
      t.boolean :internal_irb
      t.boolean :resource_study_access
      t.string :auditing_functions
      t.string :poc_first_name
      t.string :poc_last_name
      t.string :poc_title
      t.string :poc_email
      t.string :poc_phone
      t.string :altpoc_first_name
      t.string :altpoc_last_name
      t.string :altpoc_title
      t.string :altpoc_email
      t.string :altpoc_phone
      t.string :io_first_name
      t.string :io_last_name
      t.string :io_title
      t.string :io_email
      t.string :io_phone
      t.timestamps null: false
    end
  end
end
