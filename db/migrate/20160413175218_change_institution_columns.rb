class ChangeInstitutionColumns < ActiveRecord::Migration
  def change
    # Remove columns
    remove_column :institutions, :internal_irb, :boolean
    remove_column :institutions, :resource_study_access, :boolean
    remove_column :institutions, :auditing_functions, :string

    # Add columns
    add_column :institutions, :hrpp_pursuing_accreditation, :boolean
    add_column :institutions, :pursued_accrediting_organization, :string
    add_column :institutions, :pursued_accreditation_status, :string
    add_column :institutions, :ohrp_status, :string
    add_column :institutions, :hrpp_other, :boolean
    add_column :institutions, :hrpp_other_description, :string
    add_column :institutions, :no_irb, :boolean
  end
end
