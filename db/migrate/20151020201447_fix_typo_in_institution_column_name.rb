class FixTypoInInstitutionColumnName < ActiveRecord::Migration
  def change
    rename_column :institutions, :signaotry_last_downloaded_by, :signatory_last_downloaded_by
  end
end
