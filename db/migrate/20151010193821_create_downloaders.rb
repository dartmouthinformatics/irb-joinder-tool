class CreateDownloaders < ActiveRecord::Migration
  def change
    create_table :downloaders do |t|
      t.string :email
      t.integer :fwa_id
      t.datetime :downloaded_at
      t.timestamps null: false
    end
  end
end
