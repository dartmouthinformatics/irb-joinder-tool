class AddReviewingToSites < ActiveRecord::Migration
  def change
    add_column :sites, :reviewing, :boolean, null: false
  end
end
