class AddTitleToDownloaders < ActiveRecord::Migration
  def change
    add_column :downloaders, :title, :string
  end
end
