class AddRegistrationFieldsToInstitutions < ActiveRecord::Migration
  def change
    add_column :institutions, :registering_user_id, :integer
    add_column :institutions, :registered_at, :datetime
  end
end
