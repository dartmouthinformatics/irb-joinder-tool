class AddFirstLastNameToDownloaders < ActiveRecord::Migration
  def change
    add_column :downloaders, :first_name, :string
    add_column :downloaders, :last_name, :string
  end
end
