class AddContactFieldsToInstitutions < ActiveRecord::Migration
  def change
    add_column :institutions, :poc_street, :string
    add_column :institutions, :poc_city, :string
    add_column :institutions, :poc_state, :string
    add_column :institutions, :poc_zip, :string

    add_column :institutions, :altpoc_street, :string
    add_column :institutions, :altpoc_city, :string
    add_column :institutions, :altpoc_state, :string
    add_column :institutions, :altpoc_zip, :string

    add_column :institutions, :io_street, :string
    add_column :institutions, :io_city, :string
    add_column :institutions, :io_state, :string
    add_column :institutions, :io_zip, :string
  end
end
