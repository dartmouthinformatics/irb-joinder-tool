class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :subject
      t.string :to
      t.string :from
      t.string :cc
      t.string :bcc
      t.string :body
      t.timestamps null: false
    end
  end
end
