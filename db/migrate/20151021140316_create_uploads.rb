class CreateUploads < ActiveRecord::Migration
  def change
    create_table :uploads do |t|
      t.integer :user_id, null: false
      t.timestamps null: false
    end
  end
end
