class MailObserver

  # For email parameters that ay be an Array or a String or nil
  def self.stringify(obj)
    return nil unless obj
    return obj if obj.is_a?(String)
    if obj.is_a?(Array) 
      str = ''
      obj.each do |member|
        str << member + ','
      end
      return str.chop
    end
    return nil
  end

  # Mail observer that creates an Email record for every outgoing message
  def self.delivered_email(message)
		begin
			email = Email.new
			email.subject = message.subject
			email.from = stringify(message.from)
			email.to = stringify(message.to)
			email.cc = stringify(message.cc)
			email.bcc = stringify(message.bcc)
			email.body = message
			email.save
		rescue => error
      Rails.logger.error 'Email observer error:'
			Rails.logger.error error.message
 		end
  end

end

ActionMailer::Base.register_observer(MailObserver)
