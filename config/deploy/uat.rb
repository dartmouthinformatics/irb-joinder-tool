set :stage, :uat
set :branch, 'uat'
set :deploy_to, '/home/deploy/ticrely'
server 'viridis.dartmouth.edu', user: 'deploy', roles: %w(app db)
