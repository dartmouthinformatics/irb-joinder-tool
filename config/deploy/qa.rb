set :stage, :qa
set :branch, 'qa'
set :deploy_to, '/home/deploy/ticrely'
server 'caeruleus.dartmouth.edu', user: 'deploy', roles: %w(app db)
