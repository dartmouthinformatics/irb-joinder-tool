Rails.application.routes.draw do

  root 'home#index'

  devise_for :users, controllers: {registrations: 'registrations'}
  devise_scope :user do
    get 'users/sign_up/:token' => 'registrations#new_from_token'
    get 'users' => 'home#index'
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  resources 'downloaders'
  resources 'uploads'
  resources 'joinder_uploads'
  resources 'submissions'

  # Document management system disabled 
  # resources 'studies'
  # resources 'sites'
  # resources 'folders'

  patch 'save_submission_edit',  to: 'submissions#save_edit',             as: 'save_submission_edit'

  get  'edit_study_title/:id',   to: 'studies#edit_title',                as: 'edit_study_title'

  get  'study_docs/:id',         to: 'study_docs#index',                  as: 'study_docs'

  get  'thank_you',              to: 'home#thank_you',                    as: 'thank_you'
  get  'revise',                 to: 'home#revise',                       as: 'revise'
  get  'approved',               to: 'home#approved',                     as: 'approved'
  get  'rejected',               to: 'home#rejected',                     as: 'rejected'

  get  'download_upload/:id',    to: 'uploads#download',                  as: 'download_upload'
  get  'download_joinder/:id',   to: 'joinder_uploads#download',          as: 'download_joinder'

  get  'register_institution',   to: 'institutions#register',             as: 'register_institution'
  get  'save_registration',      to: 'institutions#register'
  post 'save_registration',      to: 'institutions#update'
  get  'json_institutions',      to: 'institutions#json_institutions'

  get  'offer_signatory',        to: 'downloads#offer_signatory',         as: 'offer_signatory'
  get  'download_signatory',     to: 'downloads#download_signatory',      as: 'download_signatory'
  get  'notify_signatory',       to: 'downloads#notify',                  as: 'notify_signatory'

  get  'fwa_search',             to: 'fwas#search' 
  get  'fwa_search_results',     to: 'fwas#search' 
  post 'fwa_search_results',     to: 'fwas#search_results'
  get  'fwa_select/:id',         to: 'fwas#select',                       as: 'fwa_select'
  get  'fwa_start',              to: 'fwas#start',                        as: 'fwa_start'
  get  'fwa_names',              to: 'fwas#names',                        as: 'fwa_names'
  get  'get_institution_names',  to: 'institutions#names',                as: 'institution_names'

  # ID is user-chosen FWA id
  get  'assign_institution/:id', to: 'users#assign_institution',          as: 'assign_institution'
  get  'complete_profile',       to: 'users#complete_profile',            as: 'complete_profile'
  post 'save_profile',           to: 'users#save_profile',                as: 'save_profile'
  get  'download_agreement',     to: 'users#download_agreement',          as: 'download_agreement'
  post 'contact',                to: 'users#contact',                     as: 'contact'
  post 'update_active',          to: 'users#update_active',               as: 'update_active'

  get  'admin',                  to: 'admin#index',                       as: 'admin'
  post 'invite',                 to: 'admin#invite',                      as: 'invite'

  post 'download',               to: 'downloaders#download',              as: 'download'
  post 'delete_downloader',      to: 'downloaders#delete',                as: 'delete_downloader'

end
