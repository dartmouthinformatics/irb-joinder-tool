require 'spec_helper'

RSpec.describe Fwa, type: :model do

  context 'when a valid FWA' do
    before do
      @fwa = create(:fwa)
    end

    it 'has a valid factory' do
      expect(@fwa.valid?).to eq(true)
    end

    it 'can be saved' do
      expect(@fwa.save).to eq(true)
    end
  end

  context 'when an invalid FWA' do
    before do
      @fwa = build(:invalid_fwa)
    end

    it 'has a invalid factory' do
      expect(@fwa.valid?).to eq(false)
    end

    it 'can not be saved' do
      expect(@fwa.save).to eq(false)
    end
  end

end
