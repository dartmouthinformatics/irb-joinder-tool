require 'spec_helper'

RSpec.describe Institution, type: :model do

  context 'when a valid institution' do
    before do
      @institution = build(:institution)
    end
    it 'has a valid factory' do
      expect(@institution).to be_valid
    end
    it 'has extra validation if completing profile' do
      @institution.completing_profile = true
      expect(@institution).to be_invalid
    end
    it 'has a status of nil before saving' do
      expect(@institution.status).to eq(nil)
    end
    it 'has a status of new after saving' do
      @institution.save
      expect(@institution.status).to eq('new')
    end
  end

  context 'when a completed institution' do
    before do
      @institution = build(:completed_institution)
      @institution.completing_profile = true
    end
    it 'has a valid factory' do
      expect(@institution).to be_valid
    end
    it 'has a status of nil before saving' do
      expect(@institution.status).to eq(nil)
    end
    it 'has a status of profile_complete after saving' do
      @institution.save
      expect(@institution.status).to eq('profile_complete')
    end
  end

end
