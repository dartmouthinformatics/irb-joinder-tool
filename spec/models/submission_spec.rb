require 'spec_helper'

RSpec.describe Submission, type: :model do
  context 'with a valid submission' do
    before do
      @submission  = build(:submission)
    end
    it 'has a valid factory' do
      expect(@submission).to be_valid
    end
    it 'can be saved' do
      expect(@submission.save).to be true
    end
    it 'does not need revision' do
      expect(@submission.needs_revision?).to be false
    end
    it 'needs review' do
      expect(@submission.status).to eq 'needs_review'
    end
  end
end
