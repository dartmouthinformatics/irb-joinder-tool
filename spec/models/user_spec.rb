require 'spec_helper'

RSpec.describe User, type: :model do

  context 'when a regular user' do 
    before do
      @user = build(:user)
    end
    it 'has a valid factory' do
      expect(@user.valid?).to eq(true)
    end
    it 'can be saved' do
      expect(@user.save).to eq(true)
    end
    it 'is a normal (non-admin) user' do
      @user.save
      expect(@user.role.name).to eq('user')
    end
    it 'has a status of nil before saving' do
      expect(@user.status).to eq(nil)
    end
    it 'has a status of new before saving' do
      @user.save
      expect(@user.status).to eq('new')
    end
    it 'requires a valid profile when appropriate' do
      @user.completing_profile = true
      expect(@user).to be_invalid
    end
    it 'has a status of new before saving' do
      @user.save
      expect(@user.status).to eq('new')
    end
  end

  context 'when a regular user with an institution' do
    before do
      @user = build(:user_with_institution)
    end
    it 'has a status of has_institution after saving' do
      @user.save
      expect(@user.status).to eq('has_institution')
    end
  end

  context 'when a regular user with a profile' do
    before do
      @user = build(:user_with_profile)
    end
    it 'has a status of profile_complete after saving' do
      @user.save
      expect(@user.status).to eq('profile_complete')
    end
  end

  context 'when an admin user' do 
    before do
      @admin = build(:admin)
    end
    it 'has a valid factory' do
      expect(@admin.valid?).to eq(true)
    end
    it 'can be saved' do
      expect(@admin.save).to eq(true)
    end
    it 'is an admin user' do
      @admin.save
      expect(@admin.role.name).to eq('admin')
      expect(@admin.admin?).to eq(true)
    end
    it 'has a status of nil after saving' do
      @admin.save
      expect(@admin.status).to eq(nil)
    end
  end

  context 'when a superadmin user' do 
    before do
      @superadmin = build(:superadmin)
    end
    it 'has a valid factory' do
      expect(@superadmin.valid?).to eq(true)
    end
    it 'can be saved' do
      expect(@superadmin.save).to eq(true)
    end
    it 'is a superadmin user' do
      @superadmin.save
      expect(@superadmin.role.name).to eq('superadmin')
      expect(@superadmin.superadmin?).to eq(true)
    end
    it 'has a status of nil after saving' do
      @superadmin.save
      expect(@superadmin.status).to eq(nil)
    end
  end

end
