require 'spec_helper'

RSpec.describe Upload, type: :model do

  context 'with a valid file' do
    before do
      @upload = build(:upload)
    end
    it 'has a valid factory' do
      expect(@upload).to be_valid
    end
  end

  context 'with an invalid file' do
    before do
      @upload = build(:invalid_upload)
    end
    it 'has an invalid factory' do
      expect(@upload).to be_invalid
    end
  end

end
