require 'rails_helper'

RSpec.describe Downloader, type: :model do

  before do
    @downloader = build(:downloader)
  end

  it 'has a valid factory' do
    expect(@downloader).to be_valid
  end

  it 'requires a valid email' do
    @downloader.email = 'user@'
    expect(@downloader).to be_invalid
  end

  it 'requires a first name' do
    @downloader.first_name = nil
    expect(@downloader).to be_invalid
  end

  it 'requires a last name' do
    @downloader.last_name = nil
    expect(@downloader).to be_invalid
  end

  it 'requires a title' do
    @downloader.title = nil
    expect(@downloader).to be_invalid
  end

  it 'requires an institution name' do
    @downloader.institution_name = nil
    expect(@downloader).to be_invalid
  end

end
