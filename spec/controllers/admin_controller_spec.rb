require 'rails_helper'

RSpec.describe AdminController, type: :controller do

  describe '#index' do
    login_admin
    it 'responds with status 200' do
      get :index
      puts response.status
      expect(response.status).to eq 200
    end
  end

  describe '#invite' do
    login_admin
    before do
      @downloader = create(:downloader)
    end
    it 'delievers an email to the downloader' do
      get :invite, id: @downloader.id
      email = ActionMailer::Base.deliveries.last
      expect(email.to[0]).to eq @downloader.email
    end
    it 'redirects to the root url' do
      get :invite, id: @downloader.id
      expect(response).to redirect_to :root
    end
  end

end
