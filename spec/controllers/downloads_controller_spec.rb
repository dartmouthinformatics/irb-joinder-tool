require 'rails_helper'

RSpec.describe DownloadsController, type: :controller do

  describe '#offer_signatory' do
    login_user
    it 'Assigns an instance variable of the current users institution' do
      get :offer_signatory
      expect(assigns :institution).to eq subject.current_user.institution
    end
  end

  describe '#download_signatory' do
    login_user_with_institution
    it 'Assigns an instance variable of the current users institution' do
      get :offer_signatory
      expect(assigns :institution).to eq subject.current_user.institution
    end
    it 'Sends a PDF file' do
      get :download_signatory
      expect(response.headers['Content-Type']).to eq 'application/pdf'
    end
  end

end
