require 'rails_helper'

RSpec.describe JoinderUploadsController, type: :controller do

  describe '#index' do
    login_user_with_institution
    before {@submission = create :submission}
    it 'Assigns the submission instance variable' do
      institution = subject.current_user.institution
      @submission.update_attributes(institution_id: institution.id)
      get :index
      expect(assigns :submission).to be_a Submission
    end
    it 'Assigns the uploads instance variable' do
      @submission.upload.update_attributes(user_id: subject.current_user.id)
      get :index
      expect((assigns :uploads).length).to eq 1
    end
  end

  describe '#new' do
    login_user_with_institution
    it 'Assigns the upload instance variable' do
      get :new
      expect(assigns :upload).to be_an Upload
    end
  end

  describe '#create' do
    login_user_with_institution
    it 'Creates an Upload given a valid file' do
      initial_upload_count = Upload.count
      get :create, upload: {
        doc: fixture_file_upload('valid.pdf'),
        kind: 'joinder'
      }
      uploads_created = Upload.count - initial_upload_count
      expect(uploads_created).to eq 1
    end
    it 'Does not create an Upload given an invalid file' do
      initial_upload_count = Upload.count
      get :create, upload: {
        signed_joinder: fixture_file_upload('invalid.tiff')
      }
      uploads_created = Upload.count - initial_upload_count
      expect(uploads_created).to eq 0
    end
  end

  describe '#download' do
    login_admin
    before {@upload = create :upload}
    it 'Sends the file given a valid ID' do
      get :download, id: @upload.id
      expect(response.headers['Content-Type']).to eq 'application/pdf'
    end
    it 'Redirect to root given an invalid ID' do
      get :download, id: 9999999
      expect(response).to redirect_to :root
    end
  end

end
