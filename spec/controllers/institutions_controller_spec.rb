require 'rails_helper'

RSpec.describe InstitutionsController, type: :controller do

  describe '#register' do
    login_user_with_institution
    it 'Assigns the institution instance variable' do
      get :register
      expect(assigns :institution).to eq subject.current_user.institution
    end
  end

  describe '#update' do
    login_user_with_incomplete_institution
    it 'Assigns the institution instance variable' do
      get :update, institution: {id: subject.current_user.institution.id}
      expect(assigns :institution).to eq subject.current_user.institution
    end
    it 'Assigns completing_profile to true when appropriate' do
      get :update, institution: {id: subject.current_user.institution.id}, commit: 'Save Complete Form and Continue'
      institution = assigns :institution
      expect(institution.completing_profile).to eq true
    end
    it 'Successfully updates and updates registration attributes when completing profile' do
      get :update, institution: {
        id: subject.current_user.institution.id,
        name: Faker::University.name,
        city: Faker::Address.city,
        state: Faker::Address.state_abbr,
        poc_first_name: 'Some',
        poc_last_name: 'Person',
        poc_title: 'Boss',
        poc_email: 'some_valid@email.com',
        poc_phone: '1112223333',
        altpoc_first_name: 'Some',
        altpoc_last_name: 'Person',
        altpoc_title: 'Boss',
        altpoc_email: 'another_valid@email.com',
        altpoc_phone: '1112223333',
        io_first_name: 'Some',
        io_last_name: 'Person',
        io_title: 'Boss',
        io_email: 'another_valid@email.com',
        io_phone: '1112223333',
        ohrp: true,
        university: true,
        zip: '12345',
        fwa_box_unchecked: 'yes'
      }, commit: 'Save Complete Form and Continue'
      institution = Institution.find((assigns :institution).id)
      expect(institution.registered_at).to_not be_nil
      expect(institution.registering_user_id).to_not be_nil
    end
  end

end
