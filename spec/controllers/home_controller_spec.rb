require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  describe 'regular user' do
    login_user
    it 'has a current user' do
      expect(subject.current_user).not_to be_nil
    end
    it 'can get the home page' do
      get :index
      expect(response).to redirect_to fwa_start_path 
    end
  end

  describe 'regular user with institution' do
    login_user_with_institution
    it 'has a current user' do
      expect(subject.current_user).not_to be_nil
    end
    it 'can get the home page' do
      get :index
      expect(response).to redirect_to complete_profile_path 
    end
  end

  describe 'regular user with profile' do
    # Builds user with a *complete* institution (institution profile complete)
    login_user_with_profile
    it 'has a current user' do
      expect(subject.current_user).not_to be_nil
    end
    it 'can get the home page' do
      get :index
      expect(response).to redirect_to offer_signatory_path
    end
  end

  describe 'admin user' do
    login_admin
    it 'has a current user' do
      expect(subject.current_user).not_to be_nil
    end
    it 'can get the home page' do
      get :index
      expect(response).to redirect_to admin_path 
    end
  end

end
