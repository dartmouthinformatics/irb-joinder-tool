require 'rails_helper'

RSpec.describe DownloadersController, type: :controller do

  describe '#new' do
    it 'Instantiates a new Downloader' do
      get :new
      expect(assigns[:downloader]).to be_a Downloader
    end
  end

  describe '#create' do
    it 'Creates a downloader with proper params' do
      initial_downloader_count = Downloader.count
      get :create, downloader: {
        first_name: 'First',
        last_name: 'Last',
        title: 'Boss',
        email: 'boss@downloader.com',
        institution_name: 'Downloader University'
      }
      created_downloaders = Downloader.count - initial_downloader_count
      expect(created_downloaders).to eq 1
    end
    it 'Raises an error if first_name is missing' do
      get :create, downloader: {
        last_name: 'Last',
        title: 'Boss',
        email: 'boss@downloader.com',
        institution_name: 'Downloader University'
      }
      expect(assigns[:downloader]).to be_invalid
    end
    it 'Raises an error if last_name is missing' do
      get :create, downloader: {
        first_name: 'First',
        title: 'Boss',
        email: 'boss@downloader.com',
        institution_name: 'Downloader University'
      }
      expect(assigns[:downloader]).to be_invalid
    end
    it 'Raises an error if title is missing' do
      get :create, downloader: {
        first_name: 'First',
        last_name: 'Last',
        email: 'boss@downloader.com',
        institution_name: 'Downloader University'
      }
      expect(assigns[:downloader]).to be_invalid
    end
    it 'Raises an error if email is missing' do
      get :create, downloader: {
        first_name: 'First',
        last_name: 'Last',
        title: 'Boss',
        institution_name: 'Downloader University'
      }
      expect(assigns[:downloader]).to be_invalid
    end
    it 'Raises an error if institution_name is missing' do
      get :create, downloader: {
        first_name: 'First',
        last_name: 'Last',
        title: 'Boss',
        email: 'boss@downloader.com',
      }
      expect(assigns[:downloader]).to be_invalid
    end
  end

  describe '#show' do
    it 'Assigns a Downloader instance variable given a valid ID' do
      downloader = create :downloader
      get :show, id: downloader.id
      expect(assigns[:downloader]).to be_a Downloader
    end
  end

  describe '#download' do
    before do
      @downloader = create :downloader
    end
    it 'Assigns downloaded_at for the retrieved Downloader' do
      get :download, downloader: {id: @downloader.id}
      # Must re-retrive from the db after attribute update
      downloader = Downloader.find(@downloader.id)
      expect(downloader.downloaded_at).not_to be_nil
    end
    it 'Sends the agreement pdf' do
      get :download, downloader: {id: @downloader.id}
      expect(response.body).to eq IO.binread "#{Rails.root}/agreement/IRBrely_Agreement.pdf"
    end
  end

end
