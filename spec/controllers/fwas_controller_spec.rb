require 'rails_helper'

RSpec.describe FwasController, type: :controller do

  describe '#search_results' do
    login_user
    before do
      @fwa = create(:fwa)
    end
    it 'Finds an FWA by name' do
      get :search_results, fwa: {name: @fwa.name}
      expect((assigns :fwas).length).to eq 1
    end
    it 'Finds an FWA by number' do
      get :search_results, fwa: {num: @fwa.num}
      expect((assigns :fwas).length).to eq 1
    end
    it 'Finds an FWA by city' do
      get :search_results, fwa: {city: @fwa.city}
      expect((assigns :fwas).length).to eq 1
    end
  end

  describe '#select' do
    login_user
    before do
      @fwa = create(:fwa)
    end
    it 'Finds an FWA by id' do
      get :select, id: @fwa.id
      expect(assigns :fwa).to be_a Fwa
    end
  end

end

