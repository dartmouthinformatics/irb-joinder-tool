require 'rails_helper'

RSpec.describe SubmissionsController, type: :controller do

  describe '#new' do
    login_user_with_institution
    before {@upload = create :upload}
    it 'Creates a new Submission' do
      @upload.update_attributes(user_id: subject.current_user.id)
      get :new
      expect(assigns :submission).to_not be_nil
    end
    it 'Chooses an upload for the submission' do
      @upload.update_attributes(user_id: subject.current_user.id)
      get :new
      expect(assigns :upload).to be_an Upload
    end
  end

  describe '#create' do
    login_user_with_institution
    before {@upload = create :upload}
    it 'Saves and redirects to root with valid parameters' do
      get :create, submission: {upload_id: @upload.id}
      expect(response).to redirect_to :root
    end
    it 'Renders new with invalid parameters' do
      @upload.update_attributes(user_id: subject.current_user.id)
      get :create, submission: {upload_id: nil}
      expect(response).to render_template :new
    end
  end

  describe '#update' do
    login_admin
    before {@submission = create :submission}
    it 'Updates status when appropriate' do
      # Submission must have a valid created_by id for status to be updated. 
      @submission.update_attributes(created_by: subject.current_user.id)
      # Must have an email_body when updating status
      get :update, id: @submission.id, submission: {status: 'approved'}, email_body: 'Some Text'
      # Reload from the database to get updated status
      submission = Submission.find(@submission.id)
      expect(submission.status).to eq 'approved'
    end
    it 'Upates notes when appropriate' do
      get :update, id: @submission.id, submission: {notes: 'Some Notes'} 
      submission = Submission.find(@submission.id)
      expect(submission.notes).to eq 'Some Notes'
    end
  end

  describe '#edit' do
    login_user_with_institution
    before do
      @upload = create :upload
      @submission = create :submission
    end
    it 'Sets the submission instance variable given a valid ID' do
      @upload.update_attributes(user_id: subject.current_user.id)
      get :edit, id: @submission.id
      expect(assigns :submission).to eq @submission
    end
    it 'Sets the upload instance variable given a valid ID' do
      @upload.update_attributes(user_id: subject.current_user.id)
      get :edit, id: @submission.id
      expect(assigns :upload).to eq @upload
    end
    it 'Redirect to root given an invalid ID' do
      get :edit, id: 9999
      expect(response).to redirect_to :root
    end
  end

  describe '#save_edit' do
    login_user_with_institution
    before do
      @upload = create :upload
      @submission = create :submission
    end
    it 'Updates the submission given valid parameters' do
      @upload.update_attributes(user_id: subject.current_user.id)
      get :save_edit, submission: {
        id: @submission.id,
        upload_id: @upload.id
      }
      submission = Submission.find(@submission.id)
      expect(submission.upload_id).to eq @upload.id
    end
  end

end
