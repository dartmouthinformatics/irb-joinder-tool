require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  describe '#assign_institution' do
    login_user
    before do
      @fwa = create(:fwa)
      @initial_institution_count = Institution.count
    end
    it 'creates an institution when needed' do
      get :assign_institution, id: @fwa.id
      institutions_created = Institution.count - @initial_institution_count
      expect(institutions_created).to eq 1
    end
    it 'assigns an institution_id to current_user' do
      get :assign_institution, id: @fwa.id
      expect(subject.current_user.institution_id).to_not be_nil
    end
    it 'raises an exception if not passed an id' do
      expect{get :assign_institution}.to raise_error(ActionController::UrlGenerationError)
    end
  end

  describe '#complete_profile' do
    login_user
    it 'assigns @user to current_user' do
      get :complete_profile
      expect(assigns[:user]).to eq subject.current_user
    end
    it 'creates the @degrees array' do
      get :complete_profile
      expect(assigns[:degrees]).to be_an Array 
    end
  end

  describe '#save_profile' do
    login_user
    it 'updates the user when passed all needed params' do
      get :save_profile, user: {
        first_name: 'First',
        last_name: 'Last',
        phone: '123-456-7890',
        title: 'The Boss',
        degrees: ['Md', 'PhD']
      }
      expect(subject.current_user.first_name).to eq 'First'
    end
    it 'raises an error if first_name is missing' do
      get :save_profile, user: {
        last_name: 'Last',
        phone: '123-456-7890',
        title: 'The Boss',
        degrees: ['Md', 'PhD']
      }
      expect(subject.current_user.errors.count).to eq 1
    end
    it 'raises an error if last_name is missing' do
      get :save_profile, user: {
        phone: '123-456-7890',
        title: 'The Boss',
        degrees: ['Md', 'PhD']
      }
      expect(subject.current_user.errors.count).to eq 2
    end
    it 'raises an error if phone is missing' do
      get :save_profile, user: {
        title: 'The Boss',
        degrees: ['Md', 'PhD']
      }
      expect(subject.current_user.errors.count).to eq 3
    end
    it 'raises an error if title is missing' do
      get :save_profile, user: {
        degrees: ['Md', 'PhD']
      }
      expect(subject.current_user.errors.count).to eq 4
    end
    it 'raises an error if degrees is missing' do
      get :save_profile, user: {
        dummy_param: 'dummy'
      }
      expect(subject.current_user.errors.count).to eq 5
    end
  end

  describe '#download_agreement' do
    login_user
    it 'should send the IRBrely agreement file' do
      response = get :download_agreement
      expect(response.body).to eq IO.binread "#{Rails.root}/agreement/IRBrely_Agreement.pdf"
    end
  end

end
