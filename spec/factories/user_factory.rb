FactoryGirl.define do

  factory :user do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
    confirmed_at Time.now
  end

  factory :user_with_institution, class: User  do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
    confirmed_at Time.now
    association :institution, factory: :completed_institution
  end

  factory :user_with_incomplete_institution, class: User  do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
    confirmed_at Time.now
    association :institution, factory: :institution
  end

  factory :user_with_profile, class: User do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
    first_name 'first'
    last_name 'last'
    phone '111-111-1111'
    degrees 'MD,PhD'
    title 'boss'
    confirmed_at Time.now
    association :institution, factory: :completed_institution
  end

  factory :admin, class: User do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
    role Role.find_by(name: 'admin')
    confirmed_at Time.now
  end

  factory :superadmin, class: User do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
    role Role.find_by(name: 'superadmin')
    confirmed_at Time.now
  end

end
