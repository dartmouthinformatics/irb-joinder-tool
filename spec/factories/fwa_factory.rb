FactoryGirl.define do

  factory :fwa do
    num { Faker::Number.number(10) } 
    name { Faker::University.name }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    active true
    kind 'FWA'
  end

  factory :invalid_fwa, class: Fwa do
    num { Faker::Number.number(10) } 
    name { Faker::University.name }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    kind 'FWA'
  end

end
