include ActionDispatch::TestProcess

FactoryGirl.define do

  factory :upload, class: Upload  do
    doc { fixture_file_upload(Rails.root.join('spec/fixtures/valid.pdf')) }
    kind 'joinder'
    user
  end

  factory :invalid_upload, class: Upload  do
    doc { fixture_file_upload(Rails.root.join('spec/fixtures/invalid.tiff')) }
    kind 'joinder'
    user
  end

end
