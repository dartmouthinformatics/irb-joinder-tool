FactoryGirl.define do

  factory :submission do
    created_by { Faker::Number.number(4) }
    status 'needs_review'
    upload
    association :institution, factory: :completed_institution
  end

end
