FactoryGirl.define do

  factory :institution do
    creating_user_id { Faker::Number.number(4) }
    name { Faker::University.name }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    fwa
  end

  factory :completed_institution, class: Institution do
    creating_user_id { Faker::Number.number(4) }
    name { Faker::University.name }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    poc_first_name 'Some'
    poc_last_name 'Person'
    poc_title 'Boss'
    poc_email 'some_valid@email.com'
    poc_phone '1112223333'
    altpoc_first_name 'Some'
    altpoc_last_name 'Person'
    altpoc_title 'Boss'
    altpoc_email 'another_valid@email.com'
    altpoc_phone '1112223333'
    io_first_name 'Some'
    io_last_name 'Person'
    io_title 'Boss'
    io_email 'another_valid@email.com'
    io_phone '1112223333'
    ohrp true
    university true
    zip '12345'
    fwa_box_unchecked 0
    registered_at Time.now
    fwa
  end

end
