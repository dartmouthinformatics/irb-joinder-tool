FactoryGirl.define do

  factory :downloader do
    email { Faker::Internet.email }
    first_name 'First'
    last_name 'Last'
    institution_name 'Institution'
    title 'Title'
  end

end
